# API Baby

Contact: lukasz.pili AT gmail.com


## Stack

* JPA Eclipse Link for the persistence
* Spring for the CDI
* Jersey for the REST API
* Jetty for the embedded server


## Installation

### Gradle

You need first to install gradle: http://www.gradle.org/


### Build the project

Run `gradle build`


### Configure with IDE

* Configure with IntelliJ: run `gradle idea` and open the Intellij project.
* Configure with Eclipse: run `gradle eclipse` and import the existing project into worksapce.


### Database

Runs on babyvista database.


## Run

Once gradle is installed and postgres is running:

1. Run `gradle jettyRunWar`
2. Go to localhost:8080


## API

API DOC can be found at: src/main/webapp/WEB-INF/api_doc.MD

