package com.prima.apibaby.api.exception;

import java.util.List;
import java.util.Map;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public class AppException extends RuntimeException {

    private Object entity;
    private ErrorType errorType;

    public AppException(String message, Object entity, ErrorType errorType) {
        super(message);
        this.entity = entity;
        this.errorType = errorType;
    }

    public AppException(String message, Object entity, ErrorType errorType, Throwable throwable) {
        super(message, throwable);
        this.entity = entity;
        this.errorType = errorType;
    }

    public Object getEntity() {
        return entity;
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public static enum ErrorType {
        INVALID_ARGS,
        INTERNAL,
        UNEXCPECTED,
        NOT_FOUND,
    }

    public static AppException errors(Map<String, List<String>> errors) {
        return new Builder()
                .entity(errors)
                .invalidArgs();
    }

    public static Builder withMessage(String message, String... args) {
        return new Builder()
                .message(message, args);
    }

    public static Builder withEntity(Object entity) {
        return new Builder()
                .entity(entity)
                .message("App exception");
    }

    public static Builder withEntity(Object entity, String description) {
        return new Builder()
                .entity(entity)
                .message(description);
    }



    public static class Builder {

        private String message;
        private Object entity;
        private ErrorType errorType = ErrorType.UNEXCPECTED;
        private Throwable throwable;

        public Builder message(String message, String... args) {
            this.message = String.format(message, args);
            return this;
        }

        public Builder entity(Object entity) {
            this.entity = entity;
            return this;
        }

        public Builder errorType(ErrorType errorType) {
            this.errorType = errorType;
            return this;
        }

        public Builder throwable(Throwable throwable) {
            this.throwable = throwable;
            return this;
        }

        public AppException build() {
            if (message == null && entity == null) {
                throw new IllegalArgumentException("message and entity cannot be null together");
            }

            if (throwable == null) {
                return new AppException(message, entity, errorType);
            } else {
                return new AppException(message, entity, errorType, throwable);
            }
        }

        public AppException invalidArgs() {
            this.errorType = ErrorType.INVALID_ARGS;
            return build();
        }

        public AppException internal() {
            this.errorType = ErrorType.INTERNAL;
            return build();
        }

        public AppException notFound() {
            this.errorType = ErrorType.NOT_FOUND;
            return build();
        }
    }


}
