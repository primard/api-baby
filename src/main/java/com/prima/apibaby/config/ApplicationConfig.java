package com.prima.apibaby.config;

import com.prima.apibaby.rs.mapper.AppObjectMapper;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Configuration
public class ApplicationConfig {

    @Bean
    public ObjectMapper jacksonObjectMapper() {
        return new AppObjectMapper();
    }

    @Bean
    public SerializationConfig serializationConfig() {
        return jacksonObjectMapper().getSerializationConfig();
    }
}
