package com.prima.apibaby.dao;

import com.prima.apibaby.entity.report.Baby;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public interface BabyDao extends EntityDao<Baby> {
}
