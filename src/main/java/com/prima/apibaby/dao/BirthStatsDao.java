package com.prima.apibaby.dao;

import com.prima.apibaby.entity.stats.BirthStats;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public interface BirthStatsDao extends EntityDao<BirthStats> {

    @Query("SELECT bs FROM BirthStats bs WHERE bs.maternityCode = :maternityCode " +
            "AND SQL('EXTRACT(MONTH FROM ?)', bs.referenceDate) = :month " +
            "AND SQL('EXTRACT(YEAR FROM ?)', bs.referenceDate) = :year")
    public BirthStats findByMaternityAndMonth(@Param("maternityCode") long maternityCode, @Param("month") long month, @Param("year") long year);

    @Query("SELECT bs FROM BirthStats bs WHERE SQL('EXTRACT(MONTH FROM ?)', bs.referenceDate) = :month AND SQL('EXTRACT(YEAR FROM ?)', bs.referenceDate) = :year")
    public List<BirthStats> findAllByMonthAndYear(@Param("month") long month, @Param("year") long year);
}
