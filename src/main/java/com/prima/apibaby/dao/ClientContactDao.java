package com.prima.apibaby.dao;

import com.prima.apibaby.entity.report.ClientContact;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public interface ClientContactDao extends EntityDao<ClientContact> {
}
