package com.prima.apibaby.dao;

import com.prima.apibaby.entity.report.Client;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public interface ClientDao extends EntityDao<Client> {
}
