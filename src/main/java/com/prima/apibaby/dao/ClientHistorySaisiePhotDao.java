package com.prima.apibaby.dao;

import com.prima.apibaby.entity.report.ClientHistorySaisiePhot;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public interface ClientHistorySaisiePhotDao extends EntityDao<ClientHistorySaisiePhot> {
}
