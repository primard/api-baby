package com.prima.apibaby.dao;

import com.prima.apibaby.entity.employee.Employee;
import org.springframework.data.jpa.repository.Query;

import javax.inject.Named;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Named
public interface EmployeeDao extends EntityDao<Employee> {

    @Query("SELECT e FROM Employee e JOIN e.maternities m WHERE e.id = ?1")
    public Employee findOneWithMaternities(long id);
}
