package com.prima.apibaby.dao;

import com.prima.apibaby.entity.employee.Employee;
import com.prima.apibaby.entity.employee.EmployeeMaternities;
import com.prima.apibaby.entity.report.Baby;
import org.springframework.data.jpa.repository.Query;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public interface EmployeeMaternitiesDao extends EntityDao<EmployeeMaternities> {

//    @Query("SELECT em FROM EmployeeMaternities em " +
//            "JOIN Employee e " +
//            "JOIN Maternity ")
//    public EmployeeMaternities findByEmployeeId(long id);
}
