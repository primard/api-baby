package com.prima.apibaby.dao;

import com.prima.apibaby.entity.AbstractEntity;
import com.prima.apibaby.entity.simpletype.EntityState;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public interface EntityDao<T extends AbstractEntity> extends JpaRepository<T, Long> {

    public List<T> findAllByOrderByIdDesc(Pageable pageable);

    public long countByStateAndId(EntityState state, long id);

    public T findById(long id);
}
