package com.prima.apibaby.dao;

import com.prima.apibaby.entity.report.Baby;
import com.prima.apibaby.entity.report.Establishment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.inject.Named;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Named
public interface EstablishmentDao extends EntityDao<Establishment> {

//    @Query("SELECT COUNT(e) FROM Establishment e WHERE e.active = 0 AND e.id = :id")
//    public long getCountOfActiveById(@Param("id") long id);
}
