package com.prima.apibaby.dao;

import com.prima.apibaby.entity.maternity.Maternity;
import com.prima.apibaby.entity.report.Baby;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public interface MaternityDao extends EntityDao<Maternity> {

//    @Query("SELECT COUNT(m) FROM Maternity m WHERE m.active = com.prima.apibaby.entity.simpletype.EntityState.ACTIVE AND m.id = :id")
//    public long getCountOfActiveById(@Param("id") long id);
}
