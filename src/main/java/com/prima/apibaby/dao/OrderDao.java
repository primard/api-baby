package com.prima.apibaby.dao;

import com.prima.apibaby.entity.report.Order;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public interface OrderDao extends EntityDao<Order> {
}
