package com.prima.apibaby.dao;

import com.prima.apibaby.entity.report.OrderLine;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public interface OrderLineDao extends EntityDao<OrderLine> {
}
