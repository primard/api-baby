package com.prima.apibaby.dao;

import com.prima.apibaby.entity.report.OrderTransfer;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public interface OrderTransferDao extends EntityDao<OrderTransfer> {
}
