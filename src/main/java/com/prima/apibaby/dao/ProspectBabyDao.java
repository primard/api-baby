package com.prima.apibaby.dao;

import com.prima.apibaby.entity.prospect.ProspectBaby;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public interface ProspectBabyDao extends EntityDao<ProspectBaby> {
}
