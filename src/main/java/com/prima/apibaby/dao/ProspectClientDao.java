package com.prima.apibaby.dao;

import com.prima.apibaby.entity.prospect.ProspectClient;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public interface ProspectClientDao extends EntityDao<ProspectClient> {

    //    @Query("SELECT pc FROM ProspectClient pc WHERE pc.lastname LIKE %:query%")
    public List<ProspectClient> findByLastnameContainingIgnoreCase(String lastname);
}
