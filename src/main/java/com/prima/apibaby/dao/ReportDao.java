package com.prima.apibaby.dao;

import com.prima.apibaby.entity.report.Report;
import com.prima.apibaby.entity.simpletype.EntityState;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public interface ReportDao extends EntityDao<Report> {

//    @Query("SELECT COUNT(r) FROM Report r WHERE r.state = com.prima.apibaby.entity.simpletype.EntityState.ACTIVE AND r.temporaryCode = :temporaryCode")
//    public long getCountOfActiveByTemporaryCode(@Param("temporaryCode") String temporaryCode);

    public long countByStateAndTemporaryCode(EntityState state, String temporaryCode);
}
