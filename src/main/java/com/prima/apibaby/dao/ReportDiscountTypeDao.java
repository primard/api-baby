package com.prima.apibaby.dao;

import com.prima.apibaby.entity.report.Baby;
import com.prima.apibaby.entity.report.ReportDiscountType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public interface ReportDiscountTypeDao extends EntityDao<ReportDiscountType> {

    public long countById(long id);
}
