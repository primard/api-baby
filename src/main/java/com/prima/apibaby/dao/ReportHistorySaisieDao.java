package com.prima.apibaby.dao;

import com.prima.apibaby.entity.report.ReportHistorySaisie;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public interface ReportHistorySaisieDao extends EntityDao<ReportHistorySaisie> {
}
