package com.prima.apibaby.entity;

import com.prima.apibaby.entity.simpletype.EntityState;
import com.prima.apibaby.entity.simpletype.Gender;
import com.prima.apibaby.entity.simpletype.Sexe;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.eclipse.persistence.annotations.ConversionValue;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.ObjectTypeConverter;
import org.eclipse.persistence.annotations.ObjectTypeConverters;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@MappedSuperclass
@ObjectTypeConverters({
        @ObjectTypeConverter(name = "entity_state", objectType = EntityState.class, dataType = Long.class, conversionValues = {
                @ConversionValue(objectValue = "ACTIVE", dataValue = "0"),
                @ConversionValue(objectValue = "INACTIVE", dataValue = "1")
        }),
        @ObjectTypeConverter(name = "boolean_to_int", objectType = Boolean.class, dataType = Long.class, conversionValues = {
                @ConversionValue(objectValue = "true", dataValue = "1"),
                @ConversionValue(objectValue = "false", dataValue = "0")
        }),
        @ObjectTypeConverter(name = "gender", objectType = Gender.class, dataType = Long.class, conversionValues = {
                @ConversionValue(objectValue = "MR", dataValue = "1"),
                @ConversionValue(objectValue = "MME", dataValue = "2"),
                @ConversionValue(objectValue = "MLLE", dataValue = "3"),
                @ConversionValue(objectValue = "UNKNOWN", dataValue = "0")
        }),
        @ObjectTypeConverter(name = "sexe", objectType = Sexe.class, dataType = Long.class, conversionValues = {
                @ConversionValue(objectValue = "MALE", dataValue = "1"),
                @ConversionValue(objectValue = "FEMALE", dataValue = "2")
        })
})
//@JsonSerialize(using = FakeBooleanSerializer.FakeBooleanSerializerClass.class)
//@JsonDeserialize(using = FakeBooleanSerializer.FakeBooleanDeserializerClass.class)
public abstract class AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String LAST_USER = "API BABY";

    protected Long id;
    protected EntityState state;
    protected String lastUser;

    protected AbstractEntity() {
        state = EntityState.ACTIVE;
        lastUser = LAST_USER;
    }

    public abstract Long getId();

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ETAT")
    @Convert("entity_state")
    @JsonIgnore
    public EntityState getState() {
        return state;
    }

    public void setState(EntityState state) {
        this.state = state;
    }

    @Column(name = "DERNIER_UTILISATEUR")
    @JsonIgnore
    public String getLastUser() {
        return lastUser;
    }

    public void setLastUser(String lastUser) {
        this.lastUser = lastUser;
    }
}
