package com.prima.apibaby.entity.employee;

import com.prima.apibaby.entity.AbstractEntity;
import com.prima.apibaby.entity.maternity.Maternity;

import javax.persistence.*;
import java.util.List;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Entity
@Table(name = "EMPLOYE")
@AttributeOverride(name = "id", column = @Column(name = "CODE_EMPLOYE"))
public class Employee extends AbstractEntity {

    private String firstname;
    private String lastname;

    private List<Maternity> maternities;

    @Override
    @Id
    public Long getId() {
        return id;
    }

    @Column(name = "PRENOM_EMPLOYE")
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Column(name = "NOM_EMPLOYE")
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @ManyToMany
    @JoinTable(
            name = "MATERNITE_PHOTOGRAPHE",
            joinColumns = {@JoinColumn(name = "code_maternite")},
            inverseJoinColumns = {@JoinColumn(name = "code_employe1")}
    )
    public List<Maternity> getMaternities() {
        return maternities;
    }

    public void setMaternities(List<Maternity> maternities) {
        this.maternities = maternities;
    }
}
