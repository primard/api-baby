package com.prima.apibaby.entity.employee;

import com.prima.apibaby.entity.AbstractEntity;
import com.prima.apibaby.entity.maternity.Maternity;

import javax.persistence.*;
import java.util.List;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Entity
@Table(name = "MATERNITE_PHOTOGRAPHE")
@AttributeOverride(name = "id", column = @Column(name = "CODE_MAT_PHOT"))
public class EmployeeMaternities extends AbstractEntity {

//    private Employee employee;
//    private List<Maternity> maternities;

    @Override
    @Id
    public Long getId() {
        return id;
    }

//    @OneToOne
//    @JoinColumn(name = "code_employe1")
//    public Employee getEmployee() {
//        return employee;
//    }
//
//    public void setEmployee(Employee employee) {
//        this.employee = employee;
//    }
//
//    @ManyToOne
//    @JoinColumn(name = "code_maternite")
//    public List<Maternity> getMaternities() {
//        return maternities;
//    }
//
//    public void setMaternities(List<Maternity> maternities) {
//        this.maternities = maternities;
//    }
}
