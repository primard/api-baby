package com.prima.apibaby.entity.maternity;

import com.prima.apibaby.entity.AbstractEntity;

import javax.persistence.*;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Entity
@Table(name = "FOURNISSEUR")
@AttributeOverride(name = "id", column = @Column(name = "CODE_FOURNISSEUR"))
public class Maternity extends AbstractEntity {

    private String name;

    @Override
    @Id
    public Long getId() {
        return id;
    }

    @Column(name = "NOM_FOURNISSEUR")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
