package com.prima.apibaby.entity.prospect;


import com.prima.apibaby.entity.AbstractEntity;
import com.prima.apibaby.entity.simpletype.Sexe;
import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.eclipse.persistence.annotations.Convert;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "BB_PROSPECT")
@AttributeOverride(name = "id", column = @Column(name = "ID_BB_PROSPECT"))
@SequenceGenerator(name = "baby_prospect_gen", sequenceName = "SEQ_BB_PROSPECT", allocationSize = 1)
public class ProspectBaby extends AbstractEntity {

    private Long rank;
    private String firstname;
    private String lastname;
    private Date birthDate;
    private Sexe sexe;
    private ProspectClient client;

    @Override
    @Id
    @GeneratedValue(generator = "baby_prospect_gen", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return id;
    }

    @Column(name = "RANG")
    public Long getRank() {
        return rank;
    }

    public void setRank(Long rank) {
        this.rank = rank;
    }

    @Column(name = "PRENOM")
    @NotBlank
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Column(name = "NOM")
    @NotBlank
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Column(name = "DATE_NAISSANCE")
    @Temporal(TemporalType.DATE)
    @JsonProperty("birth_date")
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Column(name = "SEXE")
    @Convert("sexe")
    public Sexe getSexe() {
        return sexe;
    }

    public void setSexe(Sexe sexe) {
        this.sexe = sexe;
    }

    @OneToOne
    @JoinColumn(name = "ID_CLIENT_PROSPECT", nullable = false)
    @JsonBackReference
    @JsonIgnore
    public ProspectClient getClient() {
        return client;
    }

    @JsonBackReference
    public void setClient(ProspectClient client) {
        this.client = client;
    }
}