package com.prima.apibaby.entity.prospect;

import com.prima.apibaby.entity.AbstractEntity;
import com.prima.apibaby.entity.simpletype.Gender;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonManagedReference;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonRootName;
import org.eclipse.persistence.annotations.Convert;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;


/**
 * The persistent class for the CLIENT database table.
 */
@Entity
@Table(name = "CLIENT_PROSPECT")
@AttributeOverride(name = "id", column = @Column(name = "ID_CLIENT_PROSPECT"))
@SequenceGenerator(name = "client_prospect_gen", sequenceName = "SEQ_CLIENT_PROSPECT", allocationSize = 1)
@JsonRootName("prospectclient")
@XmlRootElement(name = "prospectclient")
public class ProspectClient extends AbstractEntity {

    private Long originId;
    private Long clientStateId;
    private Long languageId;
    private String firstname;
    private String lastname;
    private String address1;
    private String address2;
    private String address3; // never used on client side, will receive values from building, floor, etc
    private String address4; // never used on client side
    private String postalCode;
    private String city;
    private String country;
    private String homePhoneNumber;
    private String mobilePhoneNumber;
    private String profesionnalPhoneNumber;
    private String email;
    private Date registerDate;
    private Gender gender;
    private Boolean optinCompany;
    private Boolean optinPartners;
    private Boolean optoutCompany;
    private Boolean optoutPartners;
    private ProspectBaby baby;

    // transient, will be added to address3
    private String building;
    private String floor;
    private String appartment;
    private String digicode;


    public ProspectClient() {
        originId = 23L;
        clientStateId = 0L;
        languageId = 1L;
    }

    @PrePersist
    protected void onPrePersist() {
        StringBuilder builder = new StringBuilder();
        if (StringUtils.isNotBlank(building)) {
            builder.append("{{building}}").append(building).append("{{/building}}");
        }
        if (StringUtils.isNotBlank(floor)) {
            builder.append("{{floor}}").append(floor).append("{{/floor}}");
        }
        if (StringUtils.isNotBlank(appartment)) {
            builder.append("{{appartment}}").append(appartment).append("{{/appartment}}");
        }
        if (StringUtils.isNotBlank(digicode)) {
            builder.append("{{digicode}}").append(digicode).append("{{/digicode}}");
        }
        address3 = builder.toString();
    }

    @PostLoad
    protected void onPostLoad() {
        if (StringUtils.isBlank(address3)) {
            return;
        }

        if (address3.contains("{{building}}")) {
            building = address3.substring(address3.indexOf("{{building}}") + 12, address3.indexOf("{{/building}}") - 1);
        }
    }

    @Override
    @Id
    @GeneratedValue(generator = "client_prospect_gen", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return id;
    }

    @Column(name = "ID_TYPE_PROVENANCE")
    @JsonIgnore
    public Long getOriginId() {
        return originId;
    }

    public void setOriginId(Long originId) {
        this.originId = originId;
    }

    @Column(name = "ID_ETAT_CLIENT")
    @JsonIgnore
    public Long getClientStateId() {
        return clientStateId;
    }

    public void setClientStateId(Long clientStateId) {
        this.clientStateId = clientStateId;
    }

    @Column(name = "ID_LANGAGE")
    @JsonIgnore
    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    @Column(name = "PRENOM")
    @NotBlank
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Column(name = "NOM")
    @NotBlank
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Column(name = "ADRESSE1")
    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    @Column(name = "ADRESSE2")
    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    @Column(name = "ADRESSE3")
    @JsonIgnore
    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    @Column(name = "ADRESSE4")
    @JsonIgnore
    public String getAddress4() {
        return address4;
    }

    public void setAddress4(String address4) {
        this.address4 = address4;
    }

    @Column(name = "CODE_POSTAL")
    @JsonProperty("postal_code")
    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Column(name = "VILLE")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column(name = "PAYS")
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Column(name = "NO_TELEPHONE_DOMICILE")
    @JsonProperty("home_phone_number")
    public String getHomePhoneNumber() {
        return homePhoneNumber;
    }

    public void setHomePhoneNumber(String homePhoneNumber) {
        this.homePhoneNumber = homePhoneNumber;
    }

    @Column(name = "NO_TELEPHONE_PORTABLE")
    @JsonProperty("mobile_phone_number")
    public String getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    public void setMobilePhoneNumber(String mobilePhoneNumber) {
        this.mobilePhoneNumber = mobilePhoneNumber;
    }

    @Column(name = "NO_TELEPHONE_PROFESSIONNEL")
    @JsonProperty("profesionnal_phone_number")
    public String getProfesionnalPhoneNumber() {
        return profesionnalPhoneNumber;
    }

    public void setProfesionnalPhoneNumber(String profesionnalPhoneNumber) {
        this.profesionnalPhoneNumber = profesionnalPhoneNumber;
    }

    @Column(name = "EMAIL")
    @NotBlank
    @Email
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "DATE_INSCRIPTION_SITE")
    @Temporal(TemporalType.DATE)
    @JsonProperty("register_date")
    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    @Column(name = "ID_GENRE")
    @Convert("gender")
    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Column(name = "OPTIN_SOCIETE")
    @Convert("boolean_to_int")
    @JsonProperty("optin_company")
    public Boolean getOptinCompany() {
        return optinCompany;
    }

    public void setOptinCompany(Boolean optinCompany) {
        this.optinCompany = optinCompany;
    }

    @Column(name = "OPTIN_PARTENAIRES")
    @Convert("boolean_to_int")
    @JsonProperty("optin_partners")
    public Boolean getOptinPartners() {
        return optinPartners;
    }

    public void setOptinPartners(Boolean optinPartners) {
        this.optinPartners = optinPartners;
    }

    @Column(name = "OPTOUT_SOCIETE")
    @Convert("boolean_to_int")
    @JsonProperty("optout_company")
    public Boolean getOptoutCompany() {
        return optoutCompany;
    }

    public void setOptoutCompany(Boolean optoutCompany) {
        this.optoutCompany = optoutCompany;
    }

    @Column(name = "OPTOUT_PARTENAIRES")
    @Convert("boolean_to_int")
    @JsonProperty("optout_partners")
    public Boolean getOptoutPartners() {
        return optoutPartners;
    }

    public void setOptoutPartners(Boolean optoutPartners) {
        this.optoutPartners = optoutPartners;
    }

    @OneToOne(mappedBy = "client")
    @JsonManagedReference
    public ProspectBaby getBaby() {
        return baby;
    }

    @JsonManagedReference
    public void setBaby(ProspectBaby baby) {
        this.baby = baby;
    }

    @Transient
    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    @Transient
    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    @Transient
    public String getAppartment() {
        return appartment;
    }

    public void setAppartment(String appartment) {
        this.appartment = appartment;
    }

    @Transient
    public String getDigicode() {
        return digicode;
    }

    public void setDigicode(String digicode) {
        this.digicode = digicode;
    }
}