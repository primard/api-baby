package com.prima.apibaby.entity.report;

import com.prima.apibaby.entity.AbstractEntity;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the BB database table.
 */
@Entity
@Table(name = "BB")
@AttributeOverride(name = "id", column = @Column(name = "idbb"))
@SequenceGenerator(name = "baby_gen", sequenceName = "SEQ_BB", allocationSize = 1)
public class Baby extends AbstractEntity {

    private Long clientCode;
    private Long maternityCode;
    private Date creationDate;
    private Date modificationDate;
    private Date birthDate;
    private Date birthHour;
    private Long reportId;
    private String lastname;
    private String firstname;
    private Long rank;
    private String sexe;

    @Override
    @Id
    @GeneratedValue(generator = "baby_gen", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return id;
    }

    @Column(name = "CODE_CLIENT")
    public Long getClientCode() {
        return this.clientCode;
    }

    public void setClientCode(Long clientCode) {
        this.clientCode = clientCode;
    }

    @Column(name = "CODE_MATERNITE")
    public Long getMaternityCode() {
        return this.maternityCode;
    }

    public void setMaternityCode(Long maternityCode) {
        this.maternityCode = maternityCode;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_CREATION")
    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_MODIFICATION")
    public Date getModificationDate() {
        return this.modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_NAISSANCE")
    public Date getBirthDate() {
        return this.birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "HEURE_NAISSANCE")
    public Date getBirthHour() {
        return this.birthHour;
    }

    public void setBirthHour(Date birthHour) {
        this.birthHour = birthHour;
    }

    @Column(name = "idreportage")
    public Long getReportId() {
        return this.reportId;
    }

    public void setReportId(Long reportId) {
        this.reportId = reportId;
    }

    @Column(name = "nom")
    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Column(name = "prenom")
    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Column(name = "rang")
    public Long getRank() {
        return this.rank;
    }

    public void setRank(Long rank) {
        this.rank = rank;
    }

    @Column(name = "sexe")
    public String getSexe() {
        return this.sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

}