package com.prima.apibaby.entity.report;

import com.prima.apibaby.entity.AbstractEntity;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the CLIENT database table.
 */
@Entity
@Table(name = "CLIENT")
@AttributeOverride(name = "id", column = @Column(name = "CODE_CLIENT"))
@SequenceGenerator(name = "client_gen", sequenceName = "SEQ_CODE_CLIENT", allocationSize = 1)
public class Client extends AbstractEntity {

    private Long clientModele;
    private Long clientPrincipal;
    private Long codeCompteEcrComptable;
    private Long codeFamilleClient;
    private Long codeModeleReglement;
    private Long codeRemise;
    private String codeSiretClient;
    private Long codeTarif;
    private Long codeTypeClient;
    private String codeUtilisateurClient;
    private Date dateDeCreation;
    private Date dateEnvoiBonPhotographe;
    private Date dateFusion;
    private Date dateModification;
    private Long etatDuClient;
    private Long idClientFe;
    private Long idClientPrincipal;
    private Long idClientProspect;
    private Long idClientWeb;
    private Long idEtatClient;
    private Long idLotTeleop;
    private Long idTypeErreur;
    private Long isInsereDansAnael;
    private String langage;
    private String radiationPartenaires;
    private String radiationSociete;
    private String raisonSocial;

    @Override
    @Id
    @GeneratedValue(generator = "client_gen", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return id;
    }

    @Column(name = "CLIENT_MODELE")
    public Long getClientModele() {
        return this.clientModele;
    }

    public void setClientModele(Long clientModele) {
        this.clientModele = clientModele;
    }

    @Column(name = "CLIENT_PRINCIPAL")
    public Long getClientPrincipal() {
        return this.clientPrincipal;
    }

    public void setClientPrincipal(Long clientPrincipal) {
        this.clientPrincipal = clientPrincipal;
    }

    @Column(name = "CODE_COMPTE_ECR_COMPTABLE")
    public Long getCodeCompteEcrComptable() {
        return this.codeCompteEcrComptable;
    }

    public void setCodeCompteEcrComptable(Long codeCompteEcrComptable) {
        this.codeCompteEcrComptable = codeCompteEcrComptable;
    }

    @Column(name = "CODE_FAMILLE_CLIENT")
    public Long getCodeFamilleClient() {
        return this.codeFamilleClient;
    }

    public void setCodeFamilleClient(Long codeFamilleClient) {
        this.codeFamilleClient = codeFamilleClient;
    }

    @Column(name = "CODE_MODELE_REGLEMENT")
    public Long getCodeModeleReglement() {
        return this.codeModeleReglement;
    }

    public void setCodeModeleReglement(Long codeModeleReglement) {
        this.codeModeleReglement = codeModeleReglement;
    }

    @Column(name = "CODE_REMISE")
    public Long getCodeRemise() {
        return this.codeRemise;
    }

    public void setCodeRemise(Long codeRemise) {
        this.codeRemise = codeRemise;
    }

    @Column(name = "CODE_SIRET_CLIENT")
    public String getCodeSiretClient() {
        return this.codeSiretClient;
    }

    public void setCodeSiretClient(String codeSiretClient) {
        this.codeSiretClient = codeSiretClient;
    }

    @Column(name = "CODE_TARIF")
    public Long getCodeTarif() {
        return this.codeTarif;
    }

    public void setCodeTarif(Long codeTarif) {
        this.codeTarif = codeTarif;
    }

    @Column(name = "CODE_TYPE_CLIENT")
    public Long getCodeTypeClient() {
        return this.codeTypeClient;
    }

    public void setCodeTypeClient(Long codeTypeClient) {
        this.codeTypeClient = codeTypeClient;
    }

    @Column(name = "CODE_UTILISATEUR_CLIENT")
    public String getCodeUtilisateurClient() {
        return this.codeUtilisateurClient;
    }

    public void setCodeUtilisateurClient(String codeUtilisateurClient) {
        this.codeUtilisateurClient = codeUtilisateurClient;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_DE_CREATION")
    public Date getDateDeCreation() {
        return this.dateDeCreation;
    }

    public void setDateDeCreation(Date dateDeCreation) {
        this.dateDeCreation = dateDeCreation;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_ENVOI_BON_PHOTOGRAPHE")
    public Date getDateEnvoiBonPhotographe() {
        return this.dateEnvoiBonPhotographe;
    }

    public void setDateEnvoiBonPhotographe(Date dateEnvoiBonPhotographe) {
        this.dateEnvoiBonPhotographe = dateEnvoiBonPhotographe;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_FUSION")
    public Date getDateFusion() {
        return this.dateFusion;
    }

    public void setDateFusion(Date dateFusion) {
        this.dateFusion = dateFusion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_MODIFICATION")
    public Date getDateModification() {
        return this.dateModification;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    @Column(name = "ETAT_DU_CLIENT")
    public Long getEtatDuClient() {
        return this.etatDuClient;
    }

    public void setEtatDuClient(Long etatDuClient) {
        this.etatDuClient = etatDuClient;
    }

    @Column(name = "ID_CLIENT_FE")
    public Long getIdClientFe() {
        return this.idClientFe;
    }

    public void setIdClientFe(Long idClientFe) {
        this.idClientFe = idClientFe;
    }

    @Column(name = "ID_CLIENT_PRINCIPAL")
    public Long getIdClientPrincipal() {
        return this.idClientPrincipal;
    }

    public void setIdClientPrincipal(Long idClientPrincipal) {
        this.idClientPrincipal = idClientPrincipal;
    }

    @Column(name = "ID_CLIENT_PROSPECT")
    public Long getIdClientProspect() {
        return this.idClientProspect;
    }

    public void setIdClientProspect(Long idClientProspect) {
        this.idClientProspect = idClientProspect;
    }

    @Column(name = "ID_CLIENT_WEB")
    public Long getIdClientWeb() {
        return this.idClientWeb;
    }

    public void setIdClientWeb(Long idClientWeb) {
        this.idClientWeb = idClientWeb;
    }

    @Column(name = "ID_ETAT_CLIENT")
    public Long getIdEtatClient() {
        return this.idEtatClient;
    }

    public void setIdEtatClient(Long idEtatClient) {
        this.idEtatClient = idEtatClient;
    }

    @Column(name = "ID_LOT_TELEOP")
    public Long getIdLotTeleop() {
        return this.idLotTeleop;
    }

    public void setIdLotTeleop(Long idLotTeleop) {
        this.idLotTeleop = idLotTeleop;
    }

    @Column(name = "ID_TYPE_ERREUR")
    public Long getIdTypeErreur() {
        return this.idTypeErreur;
    }

    public void setIdTypeErreur(Long idTypeErreur) {
        this.idTypeErreur = idTypeErreur;
    }

    @Column(name = "IS_INSERE_DANS_ANAEL")
    public Long getIsInsereDansAnael() {
        return this.isInsereDansAnael;
    }

    public void setIsInsereDansAnael(Long isInsereDansAnael) {
        this.isInsereDansAnael = isInsereDansAnael;
    }

    @Column(name = "langage")
    public String getLangage() {
        return this.langage;
    }

    public void setLangage(String langage) {
        this.langage = langage;
    }

    @Column(name = "RADIATION_PARTENAIRES")
    public String getRadiationPartenaires() {
        return this.radiationPartenaires;
    }

    public void setRadiationPartenaires(String radiationPartenaires) {
        this.radiationPartenaires = radiationPartenaires;
    }

    @Column(name = "RADIATION_SOCIETE")
    public String getRadiationSociete() {
        return this.radiationSociete;
    }

    public void setRadiationSociete(String radiationSociete) {
        this.radiationSociete = radiationSociete;
    }

    @Column(name = "RAISON_SOCIAL")
    public String getRaisonSocial() {
        return this.raisonSocial;
    }

    public void setRaisonSocial(String raisonSocial) {
        this.raisonSocial = raisonSocial;
    }

}