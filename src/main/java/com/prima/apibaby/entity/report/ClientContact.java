package com.prima.apibaby.entity.report;

import com.prima.apibaby.entity.AbstractEntity;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the CONTACT_CLIENT database table.
 */
@Entity
@Table(name = "CONTACT_CLIENT")
@AttributeOverride(name = "id", column = @Column(name = "CODE_CONTACT"))
@SequenceGenerator(name = "client_contact_gen", sequenceName = "SEQ_NOT", allocationSize = 1)
public class ClientContact extends AbstractEntity {

    private Long clientCode;
    private Long genderCode;
    private Long contactTypeCode;
    private String contactPostalCode;
    private Long geographicalZoneId;
    private String contactFirstname;
    private String contactLastname;
    private String complementaryName;
    private String contactEmail;
    private String contactAddress;
    private String contactAddress2;
    private String contactAddress3;
    private String contactAddress4;
    private String clientMainAddress;
    private String contactCity;
    private String contactCountry;
    private String contactClientCompany;
    private String contactMobilePhoneNumber;
    private String contactMobilePhoneNumber2;
    private String contactPhoneNumber;
    private String contactProfesionnalPhoneNumber;
    private Date birthDate;

    @Override
    @Id
    @GeneratedValue(generator = "client_contact_gen", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return id;
    }

    @Column(name = "CODE_CLIENT")
    @JsonProperty("client_code")
    public Long getClientCode() {
        return clientCode;
    }

    public void setClientCode(Long clientCode) {
        this.clientCode = clientCode;
    }

    @Column(name = "CODE_GENRE")
    @JsonProperty("gender_code")
    public Long getGenderCode() {
        return genderCode;
    }

    public void setGenderCode(Long genderCode) {
        this.genderCode = genderCode;
    }

    @Column(name = "CODE_TYPE_CONTACT")
    @JsonProperty("contact_type_code")
    public Long getContactTypeCode() {
        return contactTypeCode;
    }

    public void setContactTypeCode(Long contactTypeCode) {
        this.contactTypeCode = contactTypeCode;
    }

    @Column(name = "CODE_POSTAL_CONTACT")
    @JsonProperty("contact_postal_code")
    public String getContactPostalCode() {
        return contactPostalCode;
    }

    public void setContactPostalCode(String contactPostalCode) {
        this.contactPostalCode = contactPostalCode;
    }

    @Column(name = "ID_ZONE_GEO")
    @JsonProperty("geographical_zone_id")
    public Long getGeographicalZoneId() {
        return geographicalZoneId;
    }

    public void setGeographicalZoneId(Long geographicalZoneId) {
        this.geographicalZoneId = geographicalZoneId;
    }

    @Column(name = "PRENOM_CONTACT")
    @JsonProperty("contact_firstname")
    public String getContactFirstname() {
        return contactFirstname;
    }

    public void setContactFirstname(String contactFirstname) {
        this.contactFirstname = contactFirstname;
    }

    @Column(name = "NOM_CONTACT")
    @JsonProperty("contact_lastname")
    public String getContactLastname() {
        return contactLastname;
    }

    public void setContactLastname(String contactLastname) {
        this.contactLastname = contactLastname;
    }

    @Column(name = "COMPLEMENT_NOM")
    @JsonProperty("complementary_name")
    public String getComplementaryName() {
        return complementaryName;
    }

    public void setComplementaryName(String complementaryName) {
        this.complementaryName = complementaryName;
    }

    @Column(name = "EMAIL_CONTACT")
    @JsonProperty("contact_email")
    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    @Column(name = "ADRESSE_CONTACT")
    @JsonProperty("contact_address")
    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    @Column(name = "ADRESSE_CONTACT2")
    @JsonProperty("contact_address2")
    public String getContactAddress2() {
        return contactAddress2;
    }

    public void setContactAddress2(String contactAddress2) {
        this.contactAddress2 = contactAddress2;
    }

    @Column(name = "ADRESSE_CONTACT3")
    @JsonProperty("contact_address3")
    public String getContactAddress3() {
        return contactAddress3;
    }

    public void setContactAddress3(String contactAddress3) {
        this.contactAddress3 = contactAddress3;
    }

    @Column(name = "ADRESSE_CONTACT4")
    @JsonProperty("contact_address4")
    public String getContactAddress4() {
        return contactAddress4;
    }

    public void setContactAddress4(String contactAddress4) {
        this.contactAddress4 = contactAddress4;
    }

    @Column(name = "ADRESSE_PRINCIPALE_CLIENT")
    @JsonProperty("contact_main_address")
    public String getClientMainAddress() {
        return clientMainAddress;
    }

    public void setClientMainAddress(String clientMainAddress) {
        this.clientMainAddress = clientMainAddress;
    }

    @Column(name = "VILLE_CONTACT")
    @JsonProperty("contact_city")
    public String getContactCity() {
        return contactCity;
    }

    public void setContactCity(String contactCity) {
        this.contactCity = contactCity;
    }

    @Column(name = "PAYS_CONTACT")
    @JsonProperty("contact_country")
    public String getContactCountry() {
        return contactCountry;
    }

    public void setContactCountry(String contactCountry) {
        this.contactCountry = contactCountry;
    }

    @Column(name = "SOCIETE_CONTACT_CLIENT")
    @JsonProperty("contact_client_company")
    public String getContactClientCompany() {
        return contactClientCompany;
    }

    public void setContactClientCompany(String contactClientCompany) {
        this.contactClientCompany = contactClientCompany;
    }

    @Column(name = "NUMERO_MOBILE_CONTACT")
    @JsonProperty("contact_mobile_phone_number")
    public String getContactMobilePhoneNumber() {
        return contactMobilePhoneNumber;
    }

    public void setContactMobilePhoneNumber(String contactMobilePhoneNumber) {
        this.contactMobilePhoneNumber = contactMobilePhoneNumber;
    }

    @Column(name = "NUMERO_MOBILE_CONTACT2")
    @JsonProperty("contact_mobile_phone_number2")
    public String getContactMobilePhoneNumber2() {
        return contactMobilePhoneNumber2;
    }

    public void setContactMobilePhoneNumber2(String contactMobilePhoneNumber2) {
        this.contactMobilePhoneNumber2 = contactMobilePhoneNumber2;
    }

    @Column(name = "NUMERO_TELEPHONE_CONTACT")
    @JsonProperty("contact_phone_number")
    public String getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    public void setContactPhoneNumber(String contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }

    @Column(name = "NUMERO_TELEPHONE_PRO_CONTACT")
    @JsonProperty("contact_profesionnal_phone_number")
    public String getContactProfesionnalPhoneNumber() {
        return contactProfesionnalPhoneNumber;
    }

    public void setContactProfesionnalPhoneNumber(String contactProfesionnalPhoneNumber) {
        this.contactProfesionnalPhoneNumber = contactProfesionnalPhoneNumber;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_NAISSANCE")
    @JsonProperty("birth_date")
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
}