package com.prima.apibaby.entity.report;

import com.prima.apibaby.entity.AbstractEntity;

import javax.persistence.*;

@Entity
@Table(name = "HISTO_SAISIE_PHOT_CLIENT")
@AttributeOverride(name = "id", column = @Column(name = "ID_HISTO_SAISIE_CLIENT"))
@SequenceGenerator(name = "client_history_saisie_phot", sequenceName = "SEQ_ID_HISTO_SAISIE_CLIENT", allocationSize = 1)
public class ClientHistorySaisiePhot extends AbstractEntity {

    private Long clientId;
    private Long reportageHistorySaisieId;
    private String civility;
    private String clientFirstname;
    private String clientLastname;
    private String maidenName;
    private String clientBirthDate;
    private String langage;
    private String adress1;
    private String adress2;
    private String adress3;
    private String adress4;
    private String digicode;
    private String city;
    private String postalCode;
    private String country;
    private String geographicalZoneId;
    private String phoneNumber;
    private String mobilePhoneNumber1;
    private String mobilePhoneNumber2;
    private String professionalPhoneNumber;
    private String email;
    private String optinPartenaires;
    private String optinPrimavista;
    private String notOptoutPartenaires;
    private String notOptoutPrimavista;
    private String prenomBebe1;
    private String prenomBebe2;
    private String prenomEnfant1;
    private String prenomEnfant2;
    private String prenomEnfant3;
    private String prenomEnfant4;
    private String rangBebe1;
    private String sexeBebe1;
    private String sexeBebe2;
    private String sexeEnfant1;
    private String sexeEnfant2;
    private String sexeEnfant3;
    private String sexeEnfant4;
    private String dateNaissanceBebe1;
    private String dateNaissanceEnfant1;
    private String dateNaissanceEnfant2;
    private String dateNaissanceEnfant3;
    private String dateNaissanceEnfant4;
    private String heureNaissanceBebe1;
    private String heureNaissanceBebe2;

    @Override
    @Id
    @GeneratedValue(generator = "client_history_saisie_phot", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return id;
    }

    @Column(name = "ID_CLIENT")
    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    @Column(name = "ID_HISTO_SAISIE_REPORTAGE")
    public Long getReportageHistorySaisieId() {
        return reportageHistorySaisieId;
    }

    public void setReportageHistorySaisieId(Long reportageHistorySaisieId) {
        this.reportageHistorySaisieId = reportageHistorySaisieId;
    }

    @Column(name = "CIVILITE")
    public String getCivility() {
        return civility;
    }

    public void setCivility(String civility) {
        this.civility = civility;
    }

    @Column(name = "PRENOM_CLIENT")
    public String getClientFirstname() {
        return clientFirstname;
    }

    public void setClientFirstname(String clientFirstname) {
        this.clientFirstname = clientFirstname;
    }

    @Column(name = "NOM_CLIENT")
    public String getClientLastname() {
        return clientLastname;
    }

    public void setClientLastname(String clientLastname) {
        this.clientLastname = clientLastname;
    }

    @Column(name = "NOM_JEUNE_FILLE")
    public String getMaidenName() {
        return maidenName;
    }

    public void setMaidenName(String maidenName) {
        this.maidenName = maidenName;
    }

    @Column(name = "DATE_NAISSANCE_CLIENT")
    public String getClientBirthDate() {
        return clientBirthDate;
    }

    public void setClientBirthDate(String clientBirthDate) {
        this.clientBirthDate = clientBirthDate;
    }

    @Column(name = "LANGUE")
    public String getLangage() {
        return langage;
    }

    public void setLangage(String langage) {
        this.langage = langage;
    }

    @Column(name = "addresse1")
    public String getAdress1() {
        return adress1;
    }

    public void setAdress1(String adress1) {
        this.adress1 = adress1;
    }

    @Column(name = "addresse2")
    public String getAdress2() {
        return adress2;
    }


    public void setAdress2(String adress2) {
        this.adress2 = adress2;
    }

    @Column(name = "addresse3")
    public String getAdress3() {
        return adress3;
    }

    public void setAdress3(String adress3) {
        this.adress3 = adress3;
    }

    @Column(name = "addresse4")
    public String getAdress4() {
        return adress4;
    }

    public void setAdress4(String adress4) {
        this.adress4 = adress4;
    }

    @Column(name = "DIGICODE")
    public String getDigicode() {
        return digicode;
    }

    public void setDigicode(String digicode) {
        this.digicode = digicode;
    }

    @Column(name = "VILLE")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column(name = "CODE_POSTAL")
    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Column(name = "PAYS")
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Column(name = "ID_ZONE_GEO")
    public String getGeographicalZoneId() {
        return geographicalZoneId;
    }

    public void setGeographicalZoneId(String geographicalZoneId) {
        this.geographicalZoneId = geographicalZoneId;
    }

    @Column(name = "TEL_FIXE")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Column(name = "TEL_PORTABLE1")
    public String getMobilePhoneNumber1() {
        return mobilePhoneNumber1;
    }

    public void setMobilePhoneNumber1(String mobilePhoneNumber1) {
        this.mobilePhoneNumber1 = mobilePhoneNumber1;
    }

    @Column(name = "TEL_PORTABLE2")
    public String getMobilePhoneNumber2() {
        return mobilePhoneNumber2;
    }

    public void setMobilePhoneNumber2(String mobilePhoneNumber2) {
        this.mobilePhoneNumber2 = mobilePhoneNumber2;
    }

    @Column(name = "TEL_PROFESSIONNEL")
    public String getProfessionalPhoneNumber() {
        return professionalPhoneNumber;
    }

    public void setProfessionalPhoneNumber(String professionalPhoneNumber) {
        this.professionalPhoneNumber = professionalPhoneNumber;
    }

    @Column(name = "EMAIL")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "OPTIN_PARTENAIRES")
    public String getOptinPartenaires() {
        return optinPartenaires;
    }

    public void setOptinPartenaires(String optinPartenaires) {
        this.optinPartenaires = optinPartenaires;
    }

    @Column(name = "OPTIN_PRIMAVISTA")
    public String getOptinPrimavista() {
        return optinPrimavista;
    }

    public void setOptinPrimavista(String optinPrimavista) {
        this.optinPrimavista = optinPrimavista;
    }

    @Column(name = "PAS_OPTOUT_PARTENAIRES")
    public String getNotOptoutPartenaires() {
        return notOptoutPartenaires;
    }

    public void setNotOptoutPartenaires(String notOptoutPartenaires) {
        this.notOptoutPartenaires = notOptoutPartenaires;
    }

    @Column(name = "PAS_OPTOUT_PRIMAVISTA")
    public String getNotOptoutPrimavista() {
        return notOptoutPrimavista;
    }

    public void setNotOptoutPrimavista(String notOptoutPrimavista) {
        this.notOptoutPrimavista = notOptoutPrimavista;
    }

    @Column(name = "PRENOM_BEBE1")
    public String getPrenomBebe1() {
        return prenomBebe1;
    }

    public void setPrenomBebe1(String prenomBebe1) {
        this.prenomBebe1 = prenomBebe1;
    }

    @Column(name = "PRENOM_BEBE2")
    public String getPrenomBebe2() {
        return prenomBebe2;
    }

    public void setPrenomBebe2(String prenomBebe2) {
        this.prenomBebe2 = prenomBebe2;
    }

    @Column(name = "PRENOM_ENFANT1")
    public String getPrenomEnfant1() {
        return prenomEnfant1;
    }

    public void setPrenomEnfant1(String prenomEnfant1) {
        this.prenomEnfant1 = prenomEnfant1;
    }

    @Column(name = "PRENOM_ENFANT2")
    public String getPrenomEnfant2() {
        return prenomEnfant2;
    }

    public void setPrenomEnfant2(String prenomEnfant2) {
        this.prenomEnfant2 = prenomEnfant2;
    }

    @Column(name = "PRENOM_ENFANT3")
    public String getPrenomEnfant3() {
        return prenomEnfant3;
    }

    public void setPrenomEnfant3(String prenomEnfant3) {
        this.prenomEnfant3 = prenomEnfant3;
    }

    @Column(name = "PRENOM_ENFANT4")
    public String getPrenomEnfant4() {
        return prenomEnfant4;
    }

    public void setPrenomEnfant4(String prenomEnfant4) {
        this.prenomEnfant4 = prenomEnfant4;
    }

    @Column(name = "RANG_BEBE1")
    public String getRangBebe1() {
        return rangBebe1;
    }

    public void setRangBebe1(String rangBebe1) {
        this.rangBebe1 = rangBebe1;
    }

    @Column(name = "SEXE_BEBE1")
    public String getSexeBebe1() {
        return sexeBebe1;
    }

    public void setSexeBebe1(String sexeBebe1) {
        this.sexeBebe1 = sexeBebe1;
    }

    @Column(name = "SEXE_BEBE2")
    public String getSexeBebe2() {
        return sexeBebe2;
    }

    public void setSexeBebe2(String sexeBebe2) {
        this.sexeBebe2 = sexeBebe2;
    }

    @Column(name = "SEXE_ENFANT1")
    public String getSexeEnfant1() {
        return sexeEnfant1;
    }

    public void setSexeEnfant1(String sexeEnfant1) {
        this.sexeEnfant1 = sexeEnfant1;
    }

    @Column(name = "SEXE_ENFANT2")
    public String getSexeEnfant2() {
        return sexeEnfant2;
    }

    public void setSexeEnfant2(String sexeEnfant2) {
        this.sexeEnfant2 = sexeEnfant2;
    }

    @Column(name = "SEXE_ENFANT3")
    public String getSexeEnfant3() {
        return sexeEnfant3;
    }

    public void setSexeEnfant3(String sexeEnfant3) {
        this.sexeEnfant3 = sexeEnfant3;
    }

    @Column(name = "SEXE_ENFANT4")
    public String getSexeEnfant4() {
        return sexeEnfant4;
    }

    public void setSexeEnfant4(String sexeEnfant4) {
        this.sexeEnfant4 = sexeEnfant4;
    }

    @Column(name = "DATE_NAISSANCE_BEBE1")
    public String getDateNaissanceBebe1() {
        return dateNaissanceBebe1;
    }

    public void setDateNaissanceBebe1(String dateNaissanceBebe1) {
        this.dateNaissanceBebe1 = dateNaissanceBebe1;
    }

    @Column(name = "DATE_NAISSANCE_ENFANT1")
    public String getDateNaissanceEnfant1() {
        return dateNaissanceEnfant1;
    }

    public void setDateNaissanceEnfant1(String dateNaissanceEnfant1) {
        this.dateNaissanceEnfant1 = dateNaissanceEnfant1;
    }

    @Column(name = "DATE_NAISSANCE_ENFANT2")
    public String getDateNaissanceEnfant2() {
        return dateNaissanceEnfant2;
    }

    public void setDateNaissanceEnfant2(String dateNaissanceEnfant2) {
        this.dateNaissanceEnfant2 = dateNaissanceEnfant2;
    }

    @Column(name = "DATE_NAISSANCE_ENFANT3")
    public String getDateNaissanceEnfant3() {
        return dateNaissanceEnfant3;
    }

    public void setDateNaissanceEnfant3(String dateNaissanceEnfant3) {
        this.dateNaissanceEnfant3 = dateNaissanceEnfant3;
    }

    @Column(name = "DATE_NAISSANCE_ENFANT4")
    public String getDateNaissanceEnfant4() {
        return dateNaissanceEnfant4;
    }

    public void setDateNaissanceEnfant4(String dateNaissanceEnfant4) {
        this.dateNaissanceEnfant4 = dateNaissanceEnfant4;
    }

    @Column(name = "HEURE_NAISSANCE_BEBE1")
    public String getHeureNaissanceBebe1() {
        return heureNaissanceBebe1;
    }

    public void setHeureNaissanceBebe1(String heureNaissanceBebe1) {
        this.heureNaissanceBebe1 = heureNaissanceBebe1;
    }

    @Column(name = "HEURE_NAISSANCE_BEBE2")
    public String getHeureNaissanceBebe2() {
        return heureNaissanceBebe2;
    }

    public void setHeureNaissanceBebe2(String heureNaissanceBebe2) {
        this.heureNaissanceBebe2 = heureNaissanceBebe2;
    }
}