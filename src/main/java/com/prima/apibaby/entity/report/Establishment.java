package com.prima.apibaby.entity.report;

import com.prima.apibaby.entity.AbstractEntity;

import javax.persistence.*;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */

@Entity
@Table(name = "ETABLISSEMENT")
@AttributeOverride(name = "id", column = @Column(name = "CODE_ETABLISSEMENT"))
@SequenceGenerator(name = "establishment_gen", sequenceName = "SEQ_CODE_ETABLISSEMENT", allocationSize = 1)
public class Establishment extends AbstractEntity {

    @Override
    @Id
    @GeneratedValue(generator = "establishment_gen", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return id;
    }

}
