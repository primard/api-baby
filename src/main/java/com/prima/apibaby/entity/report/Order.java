package com.prima.apibaby.entity.report;

import com.prima.apibaby.entity.AbstractEntity;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "COMMANDE")
@AttributeOverride(name = "id", column = @Column(name = "CODE_COMMANDE"))
@SequenceGenerator(name = "order_gen", sequenceName = "SEQ_COMMANDE", allocationSize = 1)
public class Order extends AbstractEntity {

    private Long clientCode;
    private Long employeeCode;
    private Long establishmentCode;
    private Long orderStateId;
    private Long activityId;
    private Long reportId;
    private Long envelope;
    private Long orderDirection;
    private String orderStatus;
    private String vpc;

    @Override
    @Id
    @GeneratedValue(generator = "order_gen", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return id;
    }

    @Column(name = "CODE_CLIENT")
    @JsonProperty("client_code")
    public Long getClientCode() {
        return clientCode;
    }

    public void setClientCode(Long clientCode) {
        this.clientCode = clientCode;
    }

    @Column(name = "CODE_EMPLOYE")
    @JsonProperty("employee_code")
    public Long getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(Long employeeCode) {
        this.employeeCode = employeeCode;
    }

    @Column(name = "CODE_ETABLISSEMENT")
    @JsonProperty("establishment_code")
    public Long getEstablishmentCode() {
        return establishmentCode;
    }

    public void setEstablishmentCode(Long establishmentCode) {
        this.establishmentCode = establishmentCode;
    }

    @Column(name = "IDETAT_COMMANDE")
    @JsonProperty("order_state_id")
    public Long getOrderStateId() {
        return orderStateId;
    }

    public void setOrderStateId(Long orderStateId) {
        this.orderStateId = orderStateId;
    }

    @Column(name = "IDACTIVITE")
    @JsonProperty("activity_id")
    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    @Column(name = "IDREPORTAGE")
    @JsonProperty("report_id")
    public Long getReportId() {
        return reportId;
    }

    public void setReportId(Long reportId) {
        this.reportId = reportId;
    }

    @Column(name = "ENVELOPPE")
    public Long getEnvelope() {
        return envelope;
    }

    public void setEnvelope(Long envelope) {
        this.envelope = envelope;
    }

    @Column(name = "SENS_COMMANDE")
    @JsonProperty("order_direction")
    public Long getOrderDirection() {
        return orderDirection;
    }

    public void setOrderDirection(Long orderDirection) {
        this.orderDirection = orderDirection;
    }

    @Column(name = "STATUT_COMMANDE")
    @JsonProperty("order_status")
    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getVpc() {
        return vpc;
    }

    public void setVpc(String vpc) {
        this.vpc = vpc;
    }
}