package com.prima.apibaby.entity.report;

import com.prima.apibaby.entity.AbstractEntity;

import javax.persistence.*;

@Entity
@Table(name = "LIGNE_DE_COMMANDE")
@AttributeOverride(name = "id", column = @Column(name = "CODE_LIGNE_DE_COMMANDE"))
@SequenceGenerator(name = "order_line_gen", sequenceName = "SEQ_CODE_LIGNE_DE_COMMANDE", allocationSize = 1)
public class OrderLine extends AbstractEntity {

    private Long orderLineStateId;
    private Long itemCode;
    private Long orderCode;
    private Long orderLineOrder;
    private Long amountHT;
    private Long amountTTC;
    private Long unitAmountHT;
    private Long unitAmountTTC;
    private Long quantity;
    private Long emptySpace;
    private Long orderDirection;
    private Long orderLineType;
    private Long pictureType;

    @Override
    @Id
    @GeneratedValue(generator = "order_line_gen", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return id;
    }

    @Column(name = "ID_ETAT_LIGNE_DE_COMMANDE")
    public Long getOrderLineStateId() {
        return orderLineStateId;
    }

    public void setOrderLineStateId(Long orderLineStateId) {
        this.orderLineStateId = orderLineStateId;
    }

    @Column(name = "CODE_ARTICLE")
    public Long getItemCode() {
        return itemCode;
    }

    public void setItemCode(Long itemCode) {
        this.itemCode = itemCode;
    }

    @Column(name = "CODE_COMMANDE")
    public Long getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(Long orderCode) {
        this.orderCode = orderCode;
    }

    @Column(name = "ORDRE_LIGNE_COMMANDE")
    public Long getOrderLineOrder() {
        return orderLineOrder;
    }

    public void setOrderLineOrder(Long orderLineOrder) {
        this.orderLineOrder = orderLineOrder;
    }

    @Column(name = "MONTANT_HT")
    public Long getAmountHT() {
        return amountHT;
    }

    public void setAmountHT(Long amountHT) {
        this.amountHT = amountHT;
    }

    @Column(name = "MONTANT_TTC")
    public Long getAmountTTC() {
        return amountTTC;
    }

    public void setAmountTTC(Long amountTTC) {
        this.amountTTC = amountTTC;
    }

    @Column(name = "MONTANT_UNITAIRE_HT")
    public Long getUnitAmountHT() {
        return unitAmountHT;
    }

    public void setUnitAmountHT(Long unitAmountHT) {
        this.unitAmountHT = unitAmountHT;
    }

    @Column(name = "MONTANT_UNITAIRE_TTC")
    public Long getUnitAmountTTC() {
        return unitAmountTTC;
    }

    public void setUnitAmountTTC(Long unitAmountTTC) {
        this.unitAmountTTC = unitAmountTTC;
    }

    @Column(name = "QUANTITE")
    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    @Column(name = "LAISSE_PLACE")
    public Long getEmptySpace() {
        return emptySpace;
    }

    public void setEmptySpace(Long emptySpace) {
        this.emptySpace = emptySpace;
    }

    @Column(name = "SENS_COMMANDE")
    public Long getOrderDirection() {
        return orderDirection;
    }

    public void setOrderDirection(Long orderDirection) {
        this.orderDirection = orderDirection;
    }

    @Column(name = "TYPE_LIGNE_DE_COMMANDE")
    public Long getOrderLineType() {
        return orderLineType;
    }

    public void setOrderLineType(Long orderLineType) {
        this.orderLineType = orderLineType;
    }

    @Column(name = "TYPE_PHOTO")
    public Long getPictureType() {
        return pictureType;
    }

    public void setPictureType(Long pictureType) {
        this.pictureType = pictureType;
    }
}