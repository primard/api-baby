package com.prima.apibaby.entity.report;

import com.prima.apibaby.entity.AbstractEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "VIREMENT_COMMANDE")
@AttributeOverride(name = "id", column = @Column(name = "ID_VIREMENT_COMMANDE"))
@SequenceGenerator(name = "order_transfer_gen", sequenceName = "SEQ_VIREMENT_COMMANDE", allocationSize = 1)
public class OrderTransfer extends AbstractEntity {

    private Long stockLeftId;
    private Long stockArrivedId;
    private Long orderTransferReasonId;
    private Long orderStateId;
    private Long orderId;
    private Date transferDate;
    private String comment;

    @Override
    @Id
    @GeneratedValue(generator = "order_transfer_gen", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return id;
    }

    @Column(name = "ID_STOCK_DEPART")
    public Long getStockLeftId() {
        return stockLeftId;
    }

    public void setStockLeftId(Long stockLeftId) {
        this.stockLeftId = stockLeftId;
    }

    @Column(name = "ID_STOCK_ARRIVEE")
    public Long getStockArrivedId() {
        return stockArrivedId;
    }

    public void setStockArrivedId(Long stockArrivedId) {
        this.stockArrivedId = stockArrivedId;
    }

    @Column(name = "ID_MOTIF_VIREMENT_COMMANDE")
    public Long getOrderTransferReasonId() {
        return orderTransferReasonId;
    }

    public void setOrderTransferReasonId(Long orderTransferReasonId) {
        this.orderTransferReasonId = orderTransferReasonId;
    }

    @Column(name = "ID_ETAT_COMMANDE")
    public Long getOrderStateId() {
        return orderStateId;
    }

    public void setOrderStateId(Long orderStateId) {
        this.orderStateId = orderStateId;
    }

    @Column(name = "ID_COMMANDE")
    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_VIREMENT")
    public Date getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Date transferDate) {
        this.transferDate = transferDate;
    }

    @Column(name = "COMMENTAIRE")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}