package com.prima.apibaby.entity.report;

import com.prima.apibaby.entity.AbstractEntity;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;


/**
 * The persistent class for the REPORTAGE database table.
 */
@Entity
@Table(name = "REPORTAGE")
@AttributeOverride(name = "id", column = @Column(name = "IDREPORTAGE"))
@SequenceGenerator(name = "report_gen", sequenceName = "SEQ_IDREPORTAGE", allocationSize = 1)
public class Report extends AbstractEntity {

    private Long babyCode;
    private Long clientCode;
    private Long employeeCode;
    private Long establishmentCode;
    private Long maternityCode;
    private Long soucheCode;
    private String temporaryCode;
    private Long sellerId;
    private Long activityId;
    private Long laboratoryResultId;
    private Long articleTypeId;
    private Long geographicalZoneId;
    private Date entryLaboratoryDate;
    private Date pdvDate;
    private Long reportStateId;
    private String exploitables;
    private String comment;
    private Long discountId;
    private Boolean picturesExploitables;
    private Boolean urgent;

    private ClientContact clientContact;

    @Override
    @Id
    @GeneratedValue(generator = "report_gen", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return id;
    }

    @Column(name = "CODE_BB")
    @JsonProperty("baby_code")
    public Long getBabyCode() {
        return babyCode;
    }

    public void setBabyCode(Long babyCode) {
        this.babyCode = babyCode;
    }

    @Column(name = "CODE_CLIENT")
    @JsonProperty("client_code")
    public Long getClientCode() {
        return clientCode;
    }

    public void setClientCode(Long clientCode) {
        this.clientCode = clientCode;
    }

    @Column(name = "CODE_EMPLOYE")
    @JsonProperty("employee_code")
    public Long getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(Long employeeCode) {
        this.employeeCode = employeeCode;
    }

    @Column(name = "CODE_ETABLISSEMENT")
    public Long getEstablishmentCode() {
        return establishmentCode;
    }

    public void setEstablishmentCode(Long establishmentCode) {
        this.establishmentCode = establishmentCode;
    }

    @Column(name = "CODE_MATERNITE")
    public Long getMaternityCode() {
        return maternityCode;
    }

    public void setMaternityCode(Long maternityCode) {
        this.maternityCode = maternityCode;
    }

    @Column(name = "CODE_SOUCHE")
    public Long getSoucheCode() {
        return soucheCode;
    }

    public void setSoucheCode(Long soucheCode) {
        this.soucheCode = soucheCode;
    }

    @Column(name = "CODE_TEMPORAIRE")
    public String getTemporaryCode() {
        return temporaryCode;
    }

    public void setTemporaryCode(String temporaryCode) {
        this.temporaryCode = temporaryCode;
    }

    @Column(name = "CODE_VENDEUSE")
    @JsonProperty("seller_id")
    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    @Column(name = "IDACTIVITE")
    @JsonProperty("activity_id")
    @NotNull
    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    @Column(name = "IDRESULTAT_LABO")
    @JsonProperty("laboratory_result_id")
    public Long getLaboratoryResultId() {
        return laboratoryResultId;
    }

    public void setLaboratoryResultId(Long laboratoryResultId) {
        this.laboratoryResultId = laboratoryResultId;
    }

    @Column(name = "IDTYPE_ARTICLE")
    @JsonProperty("article_type_id")
    public Long getArticleTypeId() {
        return articleTypeId;
    }

    public void setArticleTypeId(Long articleTypeId) {
        this.articleTypeId = articleTypeId;
    }

    @Column(name = "ID_ZONE_GEO")
    @JsonProperty("geographical_zone_id")
    public Long getGeographicalZoneId() {
        return geographicalZoneId;
    }

    public void setGeographicalZoneId(Long geographicalZoneId) {
        this.geographicalZoneId = geographicalZoneId;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_ENT_LABO")
    public Date getEntryLaboratoryDate() {
        return entryLaboratoryDate;
    }

    public void setEntryLaboratoryDate(Date entryLaboratoryDate) {
        this.entryLaboratoryDate = entryLaboratoryDate;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_PDV")
    @NotNull
    public Date getPdvDate() {
        return pdvDate;
    }

    public void setPdvDate(Date pdvDate) {
        this.pdvDate = pdvDate;
    }

    @Column(name = "ETAT_REPORTAGE")
    @JsonProperty("report_state_id")
    public Long getReportStateId() {
        return reportStateId;
    }

    public void setReportStateId(Long reportStateId) {
        this.reportStateId = reportStateId;
    }

    @Column(name = "EXPLOITABLES")
    public String getExploitables() {
        return exploitables;
    }

    public void setExploitables(String exploitables) {
        this.exploitables = exploitables;
    }

    @Column(name = "COMMENTAIRE")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Column(name = "REMISE_COMMERCIALE")
    @JsonProperty("discount_id")
    public Long getDiscountId() {
        return discountId;
    }

    public void setDiscountId(Long discountId) {
        this.discountId = discountId;
    }

    @Column(name = "PHOTOS_EXPLOITABLES")
    @JsonProperty("pictures_exploitables")
    public Boolean getPicturesExploitables() {
        return picturesExploitables;
    }

    public void setPicturesExploitables(Boolean picturesExploitables) {
        this.picturesExploitables = picturesExploitables;
    }

    @Column(name = "urgent")
    public Boolean getUrgent() {
        return urgent;
    }

    public void setUrgent(Boolean urgent) {
        this.urgent = urgent;
    }

    public ClientContact getClientContact() {
        return clientContact;
    }

    public void setClientContact(ClientContact clientContact) {
        this.clientContact = clientContact;
    }
}