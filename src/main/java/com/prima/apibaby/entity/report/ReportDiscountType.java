package com.prima.apibaby.entity.report;

import com.prima.apibaby.entity.AbstractEntity;

import javax.persistence.*;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Entity
@Table(name = "TYPE_REMISE_REPORTAGE")
@AttributeOverride(name = "id", column = @Column(name = "IDTYPE_REMISE"))
public class ReportDiscountType extends AbstractEntity {



    @Override
    @Id
    public Long getId() {
        return id;
    }
}
