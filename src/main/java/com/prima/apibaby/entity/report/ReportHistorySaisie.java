package com.prima.apibaby.entity.report;

import com.prima.apibaby.entity.AbstractEntity;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "HISTO_SAISIE_PHOT_REPORTAGE")
@AttributeOverride(name = "id", column = @Column(name = "ID_HISTO_SAISIE_REPORTAGE"))
@SequenceGenerator(name = "report_history_saisie_gen", sequenceName = "SEQ_ID_HISTO_SAISIE_REPORTAGE", allocationSize = 1)
public class ReportHistorySaisie extends AbstractEntity {

    private Long reportId;
    private Long idHistoSaisieClient;
    private Long errorTypeId;
    private String streamDay;
    private String streamHour;
    private Date streamDate;
    private String filename;
    private String reportTemporaryCode;
    private String soucheCode;
    private String activityId;
    private String establishmentId;
    private String pdvDate;
    private String maternityId;
    private String discountId;
    private String urgent;
    private String commentaire;

    @Override
    @Id
    @GeneratedValue(generator = "report_history_saisie_gen", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return id;
    }

    @Column(name = "ID_REPORTAGE")
    public Long getReportId() {
        return reportId;
    }

    public void setReportId(Long reportId) {
        this.reportId = reportId;
    }

    @Column(name = "ID_HISTO_SAISIE_CLIENT")
    public Long getIdHistoSaisieClient() {
        return idHistoSaisieClient;
    }

    public void setIdHistoSaisieClient(Long idHistoSaisieClient) {
        this.idHistoSaisieClient = idHistoSaisieClient;
    }

    @Column(name = "ID_TYPE_ERREUR")
    public Long getErrorTypeId() {
        return errorTypeId;
    }

    public void setErrorTypeId(Long errorTypeId) {
        this.errorTypeId = errorTypeId;
    }

    @Column(name = "JOUR_FLUX")
    public String getStreamDay() {
        return streamDay;
    }

    public void setStreamDay(String streamDay) {
        this.streamDay = streamDay;
    }

    @Column(name = "HEURE_FLUX")
    public String getStreamHour() {
        return streamHour;
    }

    public void setStreamHour(String streamHour) {
        this.streamHour = streamHour;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_FLUX")
    public Date getStreamDate() {
        return streamDate;
    }

    public void setStreamDate(Date streamDate) {
        this.streamDate = streamDate;
    }

    @Column(name = "NOM_FICHIER")
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Column(name = "CODE_TEMPORAIRE_REPORTAGE")
    public String getReportTemporaryCode() {
        return reportTemporaryCode;
    }

    public void setReportTemporaryCode(String reportTemporaryCode) {
        this.reportTemporaryCode = reportTemporaryCode;
    }

    @Column(name = "CODE_SOUCHE")
    public String getSoucheCode() {
        return soucheCode;
    }

    public void setSoucheCode(String soucheCode) {
        this.soucheCode = soucheCode;
    }

    @Column(name = "ID_ACTIVITE")
    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    @Column(name = "ID_ENSEIGNE")
    public String getEstablishmentId() {
        return establishmentId;
    }

    public void setEstablishmentId(String establishmentId) {
        this.establishmentId = establishmentId;
    }

    @Column(name = "DATE_PDV")
    public String getPdvDate() {
        return pdvDate;
    }

    public void setPdvDate(String pdvDate) {
        this.pdvDate = pdvDate;
    }

    @Column(name = "ID_MATERNITE")
    public String getMaternityId() {
        return maternityId;
    }

    public void setMaternityId(String maternityId) {
        this.maternityId = maternityId;
    }

    @Column(name = "ID_REMISE")
    public String getDiscountId() {
        return discountId;
    }

    public void setDiscountId(String discountId) {
        this.discountId = discountId;
    }

    @Column(name = "URGENT")
    public String getUrgent() {
        return urgent;
    }

    public void setUrgent(String urgent) {
        this.urgent = urgent;
    }

    @Column(name = "COMMENT")
    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
}