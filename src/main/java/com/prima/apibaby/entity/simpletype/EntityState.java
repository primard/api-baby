package com.prima.apibaby.entity.simpletype;

import org.eclipse.persistence.annotations.ConversionValue;
import org.eclipse.persistence.annotations.ObjectTypeConverter;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public enum EntityState {
    ACTIVE, INACTIVE
}
