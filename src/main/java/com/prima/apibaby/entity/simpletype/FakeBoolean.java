package com.prima.apibaby.entity.simpletype;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 *         <p/>
 *         Fake boolean that translates to 0 or 1 for binay number columns in the database
 */
public enum FakeBoolean {
    TRUE, FALSE;

    public boolean getBooleanValue() {
        return this == TRUE;
    }

//    @JsonCreator
//    public static FakeBoolean forValue(String value) {
//        return FakeBoolean.valueOf(String.valueOf(value).toUpperCase());
//    }
//
//    @JsonValue
//    public String toValue() {
//        for (Entry<String, FakeBoolean> entry : namesMap.entrySet()) {
//            if (entry.getValue() == this)
//                return entry.getKey();
//        }
//
//        return null; // or fail
//    }
}
