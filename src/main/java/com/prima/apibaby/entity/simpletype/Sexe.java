package com.prima.apibaby.entity.simpletype;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public enum Sexe {
    MALE, FEMALE
}
