package com.prima.apibaby.entity.stats;

import com.prima.apibaby.entity.AbstractEntity;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Entity
@Table(name = "NAISSANCES")
@AttributeOverride(name = "id", column = @Column(name = "ID_NAISSANCES"))
@SequenceGenerator(name = "brith_stats_gen", sequenceName = "S_NAISSANCES", allocationSize = 1)
public class BirthStats extends AbstractEntity {

    private Long maternityCode;
    private Date referenceDate;
    private Long realNumber;

    @Override
    @Id
    @GeneratedValue(generator = "brith_stats_gen", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return id;
    }

    @Column(name = "CODE_MATERNITE")
    @JsonProperty("maternity_code")
    @NotNull
    public Long getMaternityCode() {
        return maternityCode;
    }

    public void setMaternityCode(Long maternityCode) {
        this.maternityCode = maternityCode;
    }

    @Column(name = "REFERENCEMENT")
    @Temporal(TemporalType.DATE)
    @JsonProperty("reference_date")
    @JsonIgnore
    public Date getReferenceDate() {
        return referenceDate;
    }

    public void setReferenceDate(Date referenceDate) {
        this.referenceDate = referenceDate;
    }

    @Column(name = "NB_REEL")
    @JsonProperty("real_number")
    @NotNull
    public Long getRealNumber() {
        return realNumber;
    }

    public void setRealNumber(Long realNumber) {
        this.realNumber = realNumber;
    }
}