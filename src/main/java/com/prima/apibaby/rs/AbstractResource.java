package com.prima.apibaby.rs;

import com.prima.apibaby.api.exception.AppException;
import com.prima.apibaby.entity.AbstractEntity;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.core.Response;
import java.util.*;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Named
public class AbstractResource {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractResource.class);

    @Inject
    private Validator validator;

    public <T extends AbstractEntity> void validateEntities(List<T> entities) {
        Map<String, List<String>> messages = new HashMap<String, List<String>>();

        for (T entity : entities) {
            if (entity == null) {
                LOG.debug("Entity to validate is null, ignore");
                continue;
            }

            Set<ConstraintViolation<T>> constraints = validator.validate(entity);
            if (constraints != null && !constraints.isEmpty()) {
                for (ConstraintViolation<T> constraint : constraints) {
                    LOG.warn("Constraint violation for {} - {} : {}", constraint.getRootBeanClass().getSimpleName(), constraint.getPropertyPath().toString(), constraint.getMessage());

                    List<String> array;
                    if (messages.containsKey(constraint.getPropertyPath().toString())) {
                        array = messages.get(constraint.getPropertyPath().toString());
                    } else {
                        array = new ArrayList<String>();
                        messages.put(constraint.getPropertyPath().toString(), array);
                    }

                    array.add(StringUtils.uncapitalize(constraint.getRootBeanClass().getSimpleName()) + "." + constraint.getPropertyPath().toString() + " " + constraint.getMessage());
                }
            }
        }

        if (!messages.isEmpty()) {
            throw AppException.errors(messages);
        }
    }

    public <T extends AbstractEntity> void validateEntities(T... entities) {
        validateEntities(Arrays.asList(entities));
    }

    protected Response responseWithRoot(String root, Object value) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(root, value);

        return Response.ok(map).build();
    }
}
