package com.prima.apibaby.rs;

import com.prima.apibaby.api.exception.AppException;
import com.prima.apibaby.entity.AbstractEntity;
import com.prima.apibaby.entity.stats.BirthStats;
import com.prima.apibaby.service.BirthStatsService;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.YearMonth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Named
@Path("/birthstats")
public class BirthStatsResource extends AbstractResource {

    private static final Logger LOG = LoggerFactory.getLogger(BirthStatsResource.class);

    @Inject
    private BirthStatsService birthStatsService;

    @GET
    @Produces("application/json")
    public Response findAllForCurrentMonth() {
        return findAllByMonth("current");
    }

    @GET
    @Path("month/{month}")
    @Produces("application/json")
    public Response findAllByMonth(@PathParam("month") String month) {
        LOG.debug("Find all by month {}", month);

        if (StringUtils.isBlank(month)) {
            month = "current";
        }

        List<BirthStats> list;
        if (StringUtils.equals("current", month)) {
            list = birthStatsService.findAllForCurrentMonth();
        } else if (isMonthString(month)) {
            long value = Long.parseLong(month);
            list = birthStatsService.findAllByMonth(value);
        } else {
            throw AppException.withMessage("Bad format for month").invalidArgs();
        }

        return Response.ok(list).build();
    }

    @GET
    @Path("maternity/{maternity}")
    @Produces("application/json")
    public Response findByMaternityForCurrentMonth(@PathParam("maternity") long maternity) {
        return findByMaternityAndMonth(maternity, "current");
    }

    @GET
    @Path("maternity/{maternity}/month/{month}")
    @Produces("application/json")
    public Response findByMaternityAndMonth(@PathParam("maternity") long maternity, @PathParam("month") String month) {

        if (StringUtils.isBlank(month)) {
            month = "current";
        }

        BirthStats entity;
        if (StringUtils.equals("current", month)) {
            entity = birthStatsService.findByMaternityForCurrentMonth(maternity);
        } else if (isMonthString(month)) {
            int value = Integer.parseInt(month);
            entity = birthStatsService.findByMaternityAndMonth(maternity, new YearMonth().withMonthOfYear(value));
        } else {
            throw AppException.withMessage("Bad format for month").invalidArgs();
        }

        return Response.ok(entity).build();
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response saveForCurrentMonth(@NotNull List<BirthStats> entities) {
        return saveForMonth("current", entities);
    }

    @POST
    @Path("month/{month}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response saveForMonth(@PathParam("month") String month, @NotNull List<BirthStats> entities) {
        validateEntities(entities);

        MonthParam monthParam = getMonthParam(month);

        if (monthParam.getType() == MonthParam.Type.CURRENT) {
            entities = birthStatsService.saveForCurrentMonth(entities);
        } else {
            entities = birthStatsService.saveForMonth(monthParam.getMonth(), entities);
        }

        return Response.ok(entities).build();
    }


    /* Utilities */

    private boolean isMonthString(String monthInput) {
        try {
            int value = Integer.parseInt(monthInput, 10);
            return value >= 1 && value <= 12;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private MonthParam getMonthParam(String param) {
        if (StringUtils.isBlank(param)) {
            param = "current";
        }

        if (StringUtils.equals("current", param)) {
            return new MonthParam(MonthParam.Type.CURRENT, null);
        } else if (isMonthString(param)) {
            int value = Integer.parseInt(param);
            return new MonthParam(MonthParam.Type.DATE, new YearMonth().withMonthOfYear(value));
        }

        throw AppException.withMessage("Bad format for month").invalidArgs();
    }

    private static class MonthParam {
        private enum Type {
            CURRENT, DATE
        }

        private Type type;
        private YearMonth month;

        private MonthParam(Type type, YearMonth month) {
            this.type = type;
            this.month = month;
        }

        public Type getType() {
            return type;
        }

        public YearMonth getMonth() {
            return month;
        }
    }

}