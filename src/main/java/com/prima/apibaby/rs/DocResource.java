package com.prima.apibaby.rs;

import com.prima.apibaby.api.exception.AppException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.markdown4j.Markdown4jProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Named
@Path("/doc")
public class DocResource {

    private static final Logger LOG = LoggerFactory.getLogger(DocResource.class);

    @Inject
    private ServletContext servletContext;

    @GET
    @Produces("text/html")
    public Response findAll() {
        File htmlFile = new File(servletContext.getRealPath("doc.html"));
        File markdownFile = new File(servletContext.getRealPath("WEB-INF/api_doc.md"));

        String html;
        try {
            html = FileUtils.readFileToString(htmlFile);
            String markdown = new Markdown4jProcessor().process(markdownFile);
            html = html.replace("{content}", markdown);
        } catch (IOException e) {
            throw AppException.withMessage("Cannot process the markdown documentation").throwable(e).build();
        }

        return Response.ok(html).build();
    }
}