package com.prima.apibaby.rs;

import com.prima.apibaby.entity.employee.Employee;
import com.prima.apibaby.service.EmployeeService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Named
@Path("/employee")
public class EmployeeResource extends AbstractResource {

    @Inject
    private EmployeeService employeeService;

    @GET
    @Path("{id}")
    @Produces("application/json")
    public Employee findById(@PathParam("id") long id) {
        return employeeService.findById(id);
    }
}
