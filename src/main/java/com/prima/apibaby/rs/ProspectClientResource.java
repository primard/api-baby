package com.prima.apibaby.rs;

import com.prima.apibaby.entity.prospect.ProspectClient;
import com.prima.apibaby.service.ProspectClientService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.*;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Named
@Path("/prospectclients")
@Consumes("application/json")
@Produces("application/json")
public class ProspectClientResource extends AbstractResource {

    private static final Logger LOG = LoggerFactory.getLogger(ProspectClientResource.class);

    @Inject
    private ProspectClientService prospectClientService;

    @Inject
    private Validator validator;

    @GET
    public Response findAll() {
        List<ProspectClient> list = prospectClientService.findLasts();
        return responseWithRoot("prospectclients", list);
    }

    @GET
    @Path("{id}")
    public Response findOne(@PathParam("id") Long id) {
        ProspectClient model = prospectClientService.findOne(id);
        return responseWithRoot("prospectclient", model);
    }

    @GET
    @Path("search/{lastname}")
    public Response searchByLastname(@PathParam("lastname") String lastname) {
        LOG.debug("Search by lastname {}", lastname);
        List<ProspectClient> list = prospectClientService.searchByLastname(lastname);

        // return in format value and tokens
        List<TypeAheadContainer<ProspectClient>> result = new ArrayList<TypeAheadContainer<ProspectClient>>();
        for (ProspectClient prospectClient : list) {
            result.add(new TypeAheadContainer<ProspectClient>(prospectClient, prospectClient.getLastname()));
        }

        return Response.ok(result).build();
    }


    @POST
    public Response create(@NotNull Map<String, ProspectClient> map) {
        ProspectClient prospectClient = map.get("prospectclient");
        LOG.debug("client: {}", prospectClient);
        validateEntities(prospectClient, prospectClient.getBaby()); // baby is optional, will be validate only if not null

        if (prospectClient.getBaby() != null) {
            validateEntities(prospectClient.getBaby());
        }

        prospectClientService.create(prospectClient);

        return responseWithRoot("prospectclient", prospectClient);
    }

    public class TypeAheadContainer<T> {
        private T entity;
        private String value;
        private Set<String> tokens;

        public TypeAheadContainer(T entity, String value) {
            this.entity = entity;
            this.value = value;

            tokens = new HashSet<String>(Arrays.asList(StringUtils.split(value)));
        }

        public T getEntity() {
            return entity;
        }

        public void setEntity(T entity) {
            this.entity = entity;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Set<String> getTokens() {
            return tokens;
        }

        public void setTokens(Set<String> tokens) {
            this.tokens = tokens;
        }
    }
}