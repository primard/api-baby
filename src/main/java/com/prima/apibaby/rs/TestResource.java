//package com.prima.apibaby.rs;
//
//import com.prima.apibaby.entity.Baby;
//import com.prima.apibaby.entity.prospect.ProspectClient;
//import com.prima.apibaby.service.BabyService;
//import com.prima.apibaby.service.ProspectClientService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import javax.inject.Inject;
//import javax.inject.Named;
//import javax.ws.rs.*;
//import javax.ws.rs.core.Response;
//import java.util.List;
//
///**
// * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
// */
//@Named
//@Path("/test")
//public class TestResource {
//
//    private static final Logger LOG = LoggerFactory.getLogger(TestResource.class);
//
//    @Inject
//    private BabyService babyService;
//
//    @Inject
//    private ProspectClientService prospectClientService;
//
//    @POST
//    @Path("/baby")
//    @Produces("application/json")
//    public Response create(@FormParam("firstName") String firstName, @FormParam("lastName") String lastName) {
//        Baby baby = babyService.create(firstName, lastName);
//        return Response.ok(baby).build();
//    }
//
//    @GET
//    @Path("/babies")
//    @Produces("application/json")
//    public Response findAll() {
//        LOG.debug("FUCK");
//        List<Baby> babies = babyService.findLasts();
//        return Response.ok(babies).build();
//    }
//
//    @GET
//    @Path("/clients")
//    @Produces("application/json")
//    public Response findClients() {
//        LOG.debug("FOOBAR");
//        List<ProspectClient> babies = prospectClientService.findLasts();
//        return Response.ok(babies).build();
//    }
//}