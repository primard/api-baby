package com.prima.apibaby.rs.annotation;

import javax.ws.rs.NameBinding;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@NameBinding
@Retention(RetentionPolicy.RUNTIME)
public @interface ResourceParamRequired {
}
