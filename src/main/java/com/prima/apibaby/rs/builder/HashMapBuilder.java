package com.prima.apibaby.rs.builder;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public class HashMapBuilder {

    private Map<String, String> map = new HashMap<String, String>();

    public HashMapBuilder entry(String key, String value) {
        map.put(key, value);
        return this;
    }

    public Map<String, String> build() {
        return map;
    }
}
