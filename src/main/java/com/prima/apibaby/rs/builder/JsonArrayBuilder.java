package com.prima.apibaby.rs.builder;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public class JsonArrayBuilder {

    private StringBuilder builder;
    private Map<Integer, JsonObjectBuilder> objectBuilders = new HashMap<Integer, JsonObjectBuilder>();

    public JsonObjectBuilder findOrCreate(int index) {
        JsonObjectBuilder objectBuilder;

        if (objectBuilders.containsKey(index)) {
            objectBuilder = objectBuilders.get(index);
        } else {
            objectBuilder = new JsonObjectBuilder();
            objectBuilders.put(index, objectBuilder);
        }

        return objectBuilder;
    }

    public String build() {
        builder = new StringBuilder("[");

        for (JsonObjectBuilder objectBuilder : objectBuilders.values()) {
            builder.append(objectBuilder.build()).append(", ");
        }

        if (builder.length() > 1) {
            builder.delete(builder.length() - 2, builder.length());
        }

        builder.append("]");

        return builder.toString();
    }


}
