package com.prima.apibaby.rs.builder;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public class JsonObjectBuilder {

    private StringBuilder builder;

    public JsonObjectBuilder() {
        builder = new StringBuilder("{");
    }

    public JsonObjectBuilder property(String name, String value) {
        builder.append(String.format("\"%s\":\"%s\", ", name, value));
        return this;
    }

    public String build() {
        if (builder.length() > 1) {
            builder.delete(builder.length() - 2, builder.length());
        }
        builder.append("}");

        return builder.toString();
    }


}
