//package com.primavista.api.rs.filter;
//
//import com.primavista.api.rs.annotation.AuthFilter;
//import com.primavista.api.service.UserTokenService;
//
//import javax.annotation.Priority;
//import javax.inject.Inject;
//import javax.inject.Named;
//import javax.ws.rs.Priorities;
//import javax.ws.rs.container.ContainerRequestContext;
//import javax.ws.rs.container.ContainerRequestFilter;
//import javax.ws.rs.ext.Provider;
//import java.io.IOException;
//
///**
// * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
// */
//@AuthFilter
//@Provider
//@Named
//public class AuthResourceFilter implements ContainerRequestFilter {
//
//    @Inject
//    private UserTokenService userTokenService;
//
//    @Override
//    public void filter(ContainerRequestContext requestContext) throws IOException {
//
//        System.out.println("FILTER " + userTokenService.test());
//
////        String token = requestContext.getFormParameters().getFirst("token");
////        if (StringUtils.isBlank(token)) {
////            throw new WebApplicationException(Response.status(Response.Status.FORBIDDEN).build());
////        }
////
////        if (!userTokenService.isValidToken(token)) {
////            throw new WebApplicationException(
////                    Response.status(Response.Status.FORBIDDEN)
////                            .entity("Token expired")
////                            .build());
////        }
//    }
//}
