package com.prima.apibaby.rs.mapper;

import com.prima.apibaby.api.exception.AppException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Provider
public class AppExceptionMapper implements ExceptionMapper<AppException> {

    private static final Logger LOG = LoggerFactory.getLogger(AppExceptionMapper.class);

    @Override
    public Response toResponse(AppException exception) {
        LOG.error("Catch map exception", exception);

        // Emberjs excpection format
        if (exception.getErrorType() == AppException.ErrorType.INVALID_ARGS && exception.getEntity() != null) {
            return Response.status(422).entity(new EmberErrorWrapper(exception.getEntity()))
                    .type(MediaType.APPLICATION_JSON_TYPE).build();
        }

        Response.Status status;
        switch (exception.getErrorType()) {
            case NOT_FOUND:
                status = Response.Status.NOT_FOUND;
                break;
            case UNEXCPECTED:
            case INTERNAL:
            default:
                status = Response.Status.INTERNAL_SERVER_ERROR;
        }

        String message = exception.getMessage();
        if (message == null) {
            message = "Unkown error";
        }

        return Response.status(status).entity(message).build();
    }

    public static class EmberErrorWrapper {
        private Object errors;

        public EmberErrorWrapper(Object errors) {
            this.errors = errors;
        }

        public Object getErrors() {
            return errors;
        }

        public void setErrors(Object errors) {
            this.errors = errors;
        }
    }
}
