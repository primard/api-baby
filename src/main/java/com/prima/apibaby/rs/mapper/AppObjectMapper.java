package com.prima.apibaby.rs.mapper;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.PropertyNamingStrategy;
import org.codehaus.jackson.map.SerializationConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public class AppObjectMapper extends ObjectMapper {

    private static final Logger LOG = LoggerFactory.getLogger(AppObjectMapper.class);

    public AppObjectMapper() {
        LOG.debug("CUSTOM OBJECT MAPPER");

        setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);

        configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        configure(DeserializationConfig.Feature.UNWRAP_ROOT_VALUE, false);
        configure(SerializationConfig.Feature.WRAP_ROOT_VALUE, true); // not working
//        configure(SerializationConfig.Feature.WRAP_ROOT_VALUE, true); // not working
//        configure(SerializationConfig.Feature.WRAP_ROOT_VALUE, true); // not working
    }
}
