package com.prima.apibaby.rs.model;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public class JsonKeyValue {

    private String key;
    private String value;

    public JsonKeyValue(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
