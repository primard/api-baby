package com.prima.apibaby.rs.model;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public class ResourceParamIdentifier {

    private String name;
    private String property;
    private int index;
    private boolean isArray;
    private boolean isArrayOfArrays;

    public ResourceParamIdentifier(String name) {
        this(name, null, -1, false, false);
    }

    public ResourceParamIdentifier(String name, String property) {
        this(name, property, -1, true, false);
    }

    public ResourceParamIdentifier(String name, String property, int index) {
        this(name, property, index, true, true);
    }

    private ResourceParamIdentifier(String name, String property, int index, boolean isArray, boolean isArrayOfArrays) {
        this.name = name;
        this.property = property;
        this.index = index;
        this.isArray = isArray;
        this.isArrayOfArrays = isArrayOfArrays;
    }

    public String getName() {
        return name;
    }

    public String getProperty() {
        return property;
    }

    public int getIndex() {
        return index;
    }

    public boolean isArray() {
        return isArray;
    }

    public boolean isArrayOfArrays() {
        return isArrayOfArrays;
    }
}
