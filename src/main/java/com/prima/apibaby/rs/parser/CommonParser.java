package com.prima.apibaby.rs.parser;

import com.prima.apibaby.api.exception.AppException;
import com.prima.apibaby.rs.model.ResourceParamIdentifier;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public class CommonParser {

    private static final Logger LOG = LoggerFactory.getLogger(CommonParser.class);

    private static final Pattern ARRAY_OF_ARRAYS_PARAM_PATTERN = Pattern.compile("^([\\w]+)\\[([\\w]+)\\]\\[([\\w]+)\\]$");
    private static final Pattern ARRAY_PARAM_PATTERN = Pattern.compile("^([\\w]+)\\[([\\w]+)\\]$");

    public static int parseInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw AppException.withMessage("Cannot parse int from %s", value).throwable(e).invalidArgs();
        }
    }

    public static boolean parseBoolean(String value) {
        try {
            return Boolean.valueOf(value);
        } catch (Exception e) {
            throw AppException.withMessage("Cannot parse boolean from %s", value).throwable(e).invalidArgs();
        }
    }

    public static DateTime parseDate(String value) {
        DateTimeFormatter dateStringFormat = DateTimeFormat.forPattern("yyyy-MM-dd");
        try {
            return dateStringFormat.parseDateTime(value);
        } catch (Exception e) {
            throw AppException.withMessage("Cannot parse date from %s", value).throwable(e).invalidArgs();
        }
    }

    public static ResourceParamIdentifier parseParameterName(String param) {
        ResourceParamIdentifier paramIdentifier = null;

        Matcher matcher = ARRAY_PARAM_PATTERN.matcher(param);
        if (matcher.matches()) {
            String name = matcher.group(1);
            String property = matcher.group(2);

            LOG.debug("Parse array param with name {} and property {}", name, property);
            paramIdentifier = new ResourceParamIdentifier(name, property);
        }

        matcher = ARRAY_OF_ARRAYS_PARAM_PATTERN.matcher(param);
        if (matcher.matches()) {
            String name = matcher.group(1);
            int index = CommonParser.parseInt(matcher.group(2));
            String property = matcher.group(3);

            LOG.debug("Parse array of arrays param with name {} index {} and property {}", name, index, property);
            paramIdentifier = new ResourceParamIdentifier(name, property, index);
        }

        return paramIdentifier;
    }
}
