//package com.primavista.api.rs.parser;
//
//import com.primavista.api.entity.*;
//import com.primavista.api.exception.AppException;
//import com.primavista.api.rs.builder.JsonArrayBuilder;
//import com.primavista.api.rs.builder.JsonObjectBuilder;
//import com.primavista.api.rs.model.ResourceParamIdentifier;
//import org.apache.commons.fileupload.FileItem;
//import org.apache.commons.fileupload.disk.DiskFileItemFactory;
//import org.apache.commons.fileupload.servlet.ServletFileUpload;
//import org.apache.commons.lang3.StringUtils;
//import org.apache.http.HttpRequest;
//import org.codehaus.jackson.map.DeserializationConfig;
//import org.codehaus.jackson.map.ObjectMapper;
//import org.codehaus.jackson.type.JavaType;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import javax.inject.Inject;
//import javax.inject.Named;
//import javax.servlet.http.HttpServletRequest;
//import javax.validation.ConstraintViolation;
//import javax.validation.Validator;
//import java.util.*;
//
///**
// * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
// */
//@Named
//public class RequestParser {
//
//    private static final Logger LOG = LoggerFactory.getLogger(RequestParser.class);
//
//    @Inject
//    private Validator validator;
//
//    private List<FileItem> parseRequest(HttpServletRequest request) {
//        if (!ServletFileUpload.isMultipartContent(request)) {
//            throw AppException.withMessage("Invalid request, is not multipart upload").invalidArgs();
//        }
//
//        ServletFileUpload fileUpload = new ServletFileUpload(new DiskFileItemFactory());
//
//        List<FileItem> items;
//        try {
//            items = fileUpload.parseRequest(request);
//        } catch (Exception e) {
//            throw AppException.withMessage("Request parsing error").throwable(e).internal();
//        }
//        if (items == null) {
//            throw AppException.withMessage("Request parsing returned no items").invalidArgs();
//        }
//
//        return items;
//    }
//
//    public List<Picture> parseCreatePicturesRequest(HttpServletRequest request) {
//        List<FileItem> fileItems = parseRequest(request);
//        List<Picture> pictures = new ArrayList<Picture>();
//
//        for (FileItem fileItem : fileItems) {
//            LOG.debug("Parse param {}", fileItem.getFieldName());
//
//            ResourceParamIdentifier paramIdentifier = CommonParser.parseParameterName(fileItem.getFieldName());
//            if (paramIdentifier == null) {
//                LOG.warn("Cannot parse parameter name: {}", fileItem.getFieldName());
//                continue;
//            }
//
//            String name = paramIdentifier.getName();
//            String property = paramIdentifier.getProperty();
//
//            if (StringUtils.isAnyBlank(name, property)) {
//                throw AppException.withMessage("Missing parameter args (name=%s; property=%s)", name, property).invalidArgs();
//            }
//
//            // file
//            if (name.equals("pictures")) {
//                if (!paramIdentifier.isArrayOfArrays() || !property.equals("file")) {
//                    LOG.warn("Pictures parameter must be of array of arrays type with file property: pictures[1][file]");
//                    continue;
//                }
//
//                Picture picture = new Picture();
//                picture.setBytes(fileItem.get());
//                picture.setFilename(fileItem.getName());
//
//                // validate the model via the Validation API JSR
//                validate(picture);
//
//                pictures.add(picture);
//            }
//        }
//
//        LOG.debug("Pictures: {}", pictures.size());
//
//        return pictures;
//    }
//
//    public Report parseCreateReportRequest(List<FileItem> fileItems) {
//
//        List<Picture> pictures = new ArrayList<Picture>();
//
//        JsonObjectBuilder reportBuilder = new JsonObjectBuilder();
//        JsonObjectBuilder customerBuilder = new JsonObjectBuilder();
//        JsonArrayBuilder babiesBuilder = new JsonArrayBuilder();
//
//        for (FileItem fileItem : fileItems) {
//            LOG.debug("Parse param {}", fileItem.getFieldName());
//
//            ResourceParamIdentifier paramIdentifier = CommonParser.parseParameterName(fileItem.getFieldName());
//            if (paramIdentifier == null) {
//                LOG.warn("Cannot parse parameter name: {}", fileItem.getFieldName());
//                continue;
//            }
//
//            String name = paramIdentifier.getName();
//            String property = paramIdentifier.getProperty();
//
//            if (StringUtils.isAnyBlank(name, property)) {
//                throw AppException.withMessage("Missing parameter args (name=%s; property=%s)", name, property).invalidArgs();
//            }
//
//            // file
//            if (name.equals("pictures")) {
//                if (!paramIdentifier.isArrayOfArrays() || !property.equals("file")) {
//                    LOG.warn("Pictures parameter must be of array of arrays type with file property: pictures[1][file]");
//                    continue;
//                }
//
//                Picture picture = new Picture();
//                picture.setBytes(fileItem.get());
//                picture.setFilename(fileItem.getName());
//                pictures.add(picture);
//
//                continue;
//            }
//
//            String value = fileItem.getString();
//            if (StringUtils.isBlank(value)) {
//                throw AppException.withMessage("Missing parameter value for name=%s and property=%s", name, property).invalidArgs();
//            }
//
//            // report
//            if (name.equals("report")) {
//                reportBuilder.property(property, value);
//            }
//            // customer
//            else if (name.equals("customer")) {
//                customerBuilder.property(property, value);
//            }
//            // babies
//            else if (name.equals("babies")) {
//                if (!paramIdentifier.isArrayOfArrays()) {
//                    LOG.warn("Babies parameter must be of array of arrays, ignore");
//                    continue;
//                }
//
//                JsonObjectBuilder babyBuilder = babiesBuilder.findOrCreate(paramIdentifier.getIndex());
//                babyBuilder.property(property, value);
//            }
//        }
//
//        String reportJson = reportBuilder.build();
//        String customerJson = customerBuilder.build();
//        String babiesJson = babiesBuilder.build();
//
//        LOG.debug("Report json: {}", reportJson);
//        LOG.debug("Customer json: {}", customerJson);
//        LOG.debug("Babies json: {}", babiesJson);
//        LOG.debug("Pictures: {}", pictures.size());
//
//        // from json to objects and validate
//        Report report = parseEntity(Report.class, reportJson);
//        validate(report);
//
//        Customer customer = parseEntity(Customer.class, customerJson);
//        validate(customer);
//
//        List<Baby> babies = parseEntities(Baby.class, babiesJson);
//        if (babies == null || babies.isEmpty()) {
//            throw AppException.withMessage("At least one baby required").invalidArgs();
//        }
//
//        for (Baby baby : babies) {
//            validate(baby);
//        }
//
//        report.setCustomer(customer);
//        report.setBabies(babies);
//        report.setPictures(pictures);
//
//        return report;
//    }
//
//    public <T extends AbstractEntity> T parseEntity(Class<T> cls, String json) {
//        ObjectMapper mapper = new ObjectMapper();
//        try {
//            return mapper.readValue(json, cls);
//        } catch (Exception e) {
//            throw AppException.withMessage("Cannot parse entity from json: %s", json).throwable(e).invalidArgs();
//        }
//    }
//
//    public <T extends AbstractEntity> List<T> parseEntities(Class<T> cls, String json) {
//        ObjectMapper mapper = new ObjectMapper();
//        JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, cls);
//        mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
//        try {
//            return mapper.readValue(json, type);
//        } catch (Exception e) {
//            throw new AppException.Builder()
//                    .message(String.format("Cannot parse entity from json: %s", json))
//                    .throwable(e)
//                    .build();
//        }
//    }
//
//    public <T extends AbstractEntity> void validate(T entity) {
//        Set<ConstraintViolation<T>> constraints = validator.validate(entity);
//        if (constraints != null && !constraints.isEmpty()) {
//            List<String> messages = new ArrayList<String>();
//            for (ConstraintViolation<T> constraint : constraints) {
//                LOG.warn("Constraint violation for {} - {} : {}", constraint.getRootBeanClass().getSimpleName(), constraint.getPropertyPath().toString(), constraint.getMessage());
//                messages.add(String.format("Invalid value for %s - %s : %s", constraint.getRootBeanClass().getSimpleName(), constraint.getPropertyPath().toString(), constraint.getMessage()));
//            }
//
//            if (!messages.isEmpty()) {
//                throw AppException.withEntity(messages, "Data validation failure").invalidArgs();
//            }
//        }
//    }
//}
