//package com.primavista.api.rs.parser;
//
//import com.primavista.api.entity.Customer;
//import com.primavista.api.entity.Report;
//import com.primavista.api.exception.AppException;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
///**
// * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
// */
//public class ResourceParser {
//
//    private static final Logger LOG = LoggerFactory.getLogger(ResourceParser.class);
//
//    public static void parseReport(Report report, String name, String value) {
//        // pdv type
//        if (name.equals("pdv_type")) {
//            Report.PdvType pdvType;
//            try {
//                pdvType = Report.PdvType.valueOf(value.toUpperCase());
//            } catch (Exception e) {
//                throw AppException.withMessage("Cannot parse pdv_type").throwable(e).invalidArgs();
//            }
//
//            report.setPdvType(pdvType);
//        }
//        // code_souche
//        else if (name.equals("code_souche")) {
//            report.setCode(value);
//        }
//        // pdv_date
//        else if (name.equals("pdv_date")) {
//            LOG.debug("before date");
//            report.setPdvDate(CommonParser.parseDate(value));
//            LOG.debug("after date");
//        }
//        // pdv_etablissement
//        else if (name.equals("pdv_etablissement")) {
//            report.setPdvEtablissement(value);
//        }
//        // urgent
//        else if (name.equals("urgent")) {
//            report.setUrgent(CommonParser.parseBoolean(value));
//        }
//        // comment
//        else if (name.equals("comment")) {
//            report.setComment(value);
//        }
//    }
//
//    public static void parseCustomer(Customer customer, String name, String value) {
//        // civility
//        if (name.equals("civility")) {
//            Customer.Civility civility;
//            try {
//                civility = Customer.Civility.valueOf(value.toUpperCase());
//            } catch (Exception e) {
//                throw AppException.withMessage("Cannot parse civility").throwable(e).invalidArgs();
//            }
//
//            customer.setCivility(civility);
//        }
//        // last_name
//        else if (name.equals("last_name")) {
//            customer.setLastName(value);
//        }
//        // first_name
//        else if (name.equals("first_name")) {
//            customer.setFirstName(value);
//        }
//        // receive_partners_emails
//        else if (name.equals("receive_partners_emails")) {
//            customer.setReceivePartnersEmails(CommonParser.parseBoolean(value));
//        }
//        // receive_prima_emails
//        else if (name.equals("receive_prima_emails")) {
//            customer.setReceivePrimaEmails(CommonParser.parseBoolean(value));
//        }
//        // accept_prospection
//        else if (name.equals("accept_prospection")) {
//            customer.setAcceptProspection(CommonParser.parseBoolean(value));
//        }
//        // accept_transaction_partners
//        else if (name.equals("accept_transaction_partners")) {
//            customer.setAcceptTransactionPartners(CommonParser.parseBoolean(value));
//        }
//    }
//}
