//package com.prima.apibaby.rs.serializer;
//
//import com.prima.apibaby.entity.simpletype.FakeBoolean;
//import org.codehaus.jackson.JsonGenerator;
//import org.codehaus.jackson.JsonParser;
//import org.codehaus.jackson.JsonProcessingException;
//import org.codehaus.jackson.map.DeserializationContext;
//import org.codehaus.jackson.map.JsonDeserializer;
//import org.codehaus.jackson.map.JsonSerializer;
//import org.codehaus.jackson.map.SerializerProvider;
//
//import java.io.IOException;
//
///**
// * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
// */
//public class FakeBooleanSerializer {
//
//    public class FakeBooleanSerializerClass extends JsonSerializer<FakeBoolean> {
//        @Override
//        public void serialize(FakeBoolean value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
//            jgen.writeBoolean(value.getBooleanValue());
//        }
//    }
//
//    public class FakeBooleanDeserializerClass extends JsonDeserializer<FakeBoolean> {
//        @Override
//        public FakeBoolean deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
//            boolean value = jp.getBooleanValue();
//            return FakeBoolean.valueOf(String.valueOf(value).toUpperCase());
//        }
//    }
//}
