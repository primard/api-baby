package com.prima.apibaby.rs.util;

import org.eclipse.persistence.oxm.JSONWithPadding;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public class ResponseUtil {

    public static Response buildResponse(Object entity, boolean jsonp) {
        if (jsonp) {
            entity = new JSONWithPadding<Object>(entity);
        }

        return Response.ok(entity, MediaType.APPLICATION_JSON_TYPE).build();
    }

    public static Response buildResponse(Response response, boolean jsonp) {
        if (jsonp) {
            return Response.status(response.getStatus())
                    .entity(new JSONWithPadding<Object>(response.getEntity()))
                    .type(response.getMediaType())
                    .build();
        }

        return response;
    }
}