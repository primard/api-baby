//package com.prima.apibaby.service;
//
//import com.prima.apibaby.dao.BabyDao;
//import com.prima.apibaby.entity.Baby;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.transaction.annotation.Transactional;
//
//import javax.inject.Inject;
//import javax.inject.Named;
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import javax.persistence.Query;
//import java.math.BigDecimal;
//import java.util.List;
//
///**
// * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
// */
//@Named
//public class BabyService extends EntityService<Baby, BabyDao> {
//
//    private static final Logger LOG = LoggerFactory.getLogger(BabyService.class);
//
//    @Inject
//    BabyDao babyDao;
//
//    @Transactional
//    public Baby create(String firstName, String lastName) {
//        Baby baby = new Baby();
////        baby.setId(generateId());
//        baby.setFirstName(firstName);
//        baby.setLastName(lastName);
//        baby.setClientCode(new BigDecimal(1234));
//
//        baby = babyDao.save(baby);
//
//        return baby;
//    }
//
//    @Override
//    protected BabyDao getDao() {
//        return babyDao;
//    }
//
////    @Override
////    protected String getIdSequenceName() {
////        return "SEQ_BB";
////    }
//}
