package com.prima.apibaby.service;

import com.prima.apibaby.dao.BirthStatsDao;
import com.prima.apibaby.entity.stats.BirthStats;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.YearMonth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Named
public class BirthStatsService extends EntityService<BirthStats, BirthStatsDao> {

    private static final Logger LOG = LoggerFactory.getLogger(BirthStatsService.class);

    @Inject
    private BirthStatsDao dao;

    @Override
    protected BirthStatsDao getDao() {
        return dao;
    }

    @Override
    public BirthStats create(BirthStats entity) {
        throw new UnsupportedOperationException();
    }

    @Transactional
    public BirthStats saveForCurrentMonth(BirthStats entity) {
        return saveForMonth(new YearMonth(), entity);
    }

    @Transactional
    public BirthStats saveForMonth(YearMonth month, BirthStats entity) {
        LOG.debug("Save birth stats");

        entity.setReferenceDate(month.toLocalDate(1).dayOfMonth().withMaximumValue().toDate());

        BirthStats existing = findByMaternityAndMonth(entity.getMaternityCode(), month);
        if (existing != null) {
            LOG.debug("Birth stats exists for current month, update existing one");
            existing.setRealNumber(entity.getRealNumber());
            entity = existing;
        }

        dao.save(entity);

        return entity;
    }

    @Transactional
    public List<BirthStats> saveForCurrentMonth(List<BirthStats> entities) {
        return saveForMonth(new YearMonth(), entities);
    }

    @Transactional
    public List<BirthStats> saveForMonth(YearMonth month, List<BirthStats> entities) {
        LOG.debug("Save birth stats list");

        for (BirthStats entity : entities) {
            saveForMonth(month, entity);
        }

        return entities;
    }

    public BirthStats findByMaternityForCurrentMonth(long maternityCode) {
        return findByMaternityAndMonth(maternityCode, new YearMonth());
    }

    public BirthStats findByMaternityAndMonth(long maternityCode, YearMonth month) {
        return dao.findByMaternityAndMonth(maternityCode, month.getMonthOfYear(), month.getYear());
    }

    public List<BirthStats> findAllForCurrentMonth() {
        DateTime today = new DateTime();
        return findAllByMonth(today.getMonthOfYear());
    }

    public List<BirthStats> findAllByMonth(long month) {
        DateTime today = new DateTime();
        return dao.findAllByMonthAndYear(month, today.getYear());
    }
}
