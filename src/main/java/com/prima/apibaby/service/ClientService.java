//package com.prima.apibaby.service;
//
//import com.prima.apibaby.dao.ClientDao;
//import com.prima.apibaby.entity.Client;
//import com.prima.apibaby.entity.EntityState;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.transaction.annotation.Transactional;
//
//import javax.inject.Inject;
//import javax.inject.Named;
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import java.math.BigDecimal;
//
///**
// * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
// */
//@Named
//public class ClientService extends EntityService<Client, ClientDao> {
//
//    private static final Logger LOG = LoggerFactory.getLogger(ClientService.class);
//
//    @Inject
//    private ClientDao clientDao;
//
//    @PersistenceContext
//    protected EntityManager entityManager;
//
//
//    @Transactional
//    public Client create() {
//        Client client = new Client();
////        client.setId(generateId());
//        client.setRaisonSocial("Foo");
//        client.setCodeUtilisateurClient("bar");
//        client.setEtatDuClient(new BigDecimal(1));
//        client.setClientModele(new BigDecimal(1));
//
//        client.setState(EntityState.ACTIVE);
//
//        return clientDao.save(client);
//    }
//
//    @Override
//    protected ClientDao getDao() {
//        return clientDao;
//    }
//
////    @Override
////    protected String getIdSequenceName() {
////        return "SEQ_CODE_CLIENT";
////    }
//}
