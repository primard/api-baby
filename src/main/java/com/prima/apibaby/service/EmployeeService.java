package com.prima.apibaby.service;

import com.prima.apibaby.dao.EmployeeDao;
import com.prima.apibaby.entity.employee.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Named
public class EmployeeService extends EntityService<Employee, EmployeeDao> {

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeService.class);

    @Inject
    private EmployeeDao employeeDao;

    @Override
    protected EmployeeDao getDao() {
        return employeeDao;
    }

    public Employee findById(long id) {

        Employee employee = employeeDao.findOneWithMaternities(id);
        LOG.debug("Emp {}", employee);
        return employee;
    }
}
