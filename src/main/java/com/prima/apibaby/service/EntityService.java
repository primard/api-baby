package com.prima.apibaby.service;

import com.prima.apibaby.dao.EntityDao;
import com.prima.apibaby.entity.AbstractEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
public abstract class EntityService<T_Model extends AbstractEntity, T_Dao extends EntityDao<T_Model>> {

    private static final Logger LOG = LoggerFactory.getLogger(EntityService.class);

    protected abstract T_Dao getDao();

    @PersistenceContext
    protected EntityManager entityManager;

    @Transactional
    public T_Model create(T_Model entity) {
        getDao().save(entity);
        return entity;
    }

    public List<T_Model> findLasts(int limit) {
        return getDao().findAllByOrderByIdDesc(new PageRequest(0, limit));
    }

    public List<T_Model> findLasts() {
        return findLasts(50);
    }

    public T_Model findOne(Long id) {
        return getDao().findById(id);
    }
}
