package com.prima.apibaby.service;

import com.prima.apibaby.dao.ProspectBabyDao;
import com.prima.apibaby.entity.prospect.ProspectBaby;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Named
public class ProspectBabyService extends EntityService<ProspectBaby, ProspectBabyDao> {

    @Inject
    private ProspectBabyDao dao;

    @Override
    protected ProspectBabyDao getDao() {
        return dao;
    }
}
