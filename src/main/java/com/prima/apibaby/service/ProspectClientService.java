package com.prima.apibaby.service;

import com.prima.apibaby.dao.ProspectClientDao;
import com.prima.apibaby.entity.prospect.ProspectClient;
import com.prima.apibaby.entity.simpletype.Sexe;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Named
public class ProspectClientService extends EntityService<ProspectClient, ProspectClientDao> {

    private static final Logger LOG = LoggerFactory.getLogger(ProspectClientService.class);

    @Inject
    private ProspectClientDao dao;

    @Inject
    private ProspectBabyService babyService;

    @Transactional
    public ProspectClient create(ProspectClient prospectClient) {
        dao.save(prospectClient);
        LOG.debug("Prospect client saved {}", prospectClient.getId());

        if (prospectClient.getBaby() != null) {
            prospectClient.getBaby().setSexe(Sexe.MALE);
            prospectClient.getBaby().setRank(1L);
            prospectClient.getBaby().setBirthDate(new Date());


            prospectClient.getBaby().setClient(prospectClient);
            babyService.create(prospectClient.getBaby());
        }

        return prospectClient;
    }

    public List<ProspectClient> searchByLastname(String lastname) {
        return dao.findByLastnameContainingIgnoreCase(lastname);
    }

    @Override
    protected ProspectClientDao getDao() {
        return dao;
    }
}
