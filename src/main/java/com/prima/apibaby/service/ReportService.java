package com.prima.apibaby.service;

import com.prima.apibaby.api.exception.AppException;
import com.prima.apibaby.dao.EstablishmentDao;
import com.prima.apibaby.dao.MaternityDao;
import com.prima.apibaby.dao.ReportDao;
import com.prima.apibaby.dao.ReportDiscountTypeDao;
import com.prima.apibaby.entity.report.Report;
import com.prima.apibaby.entity.simpletype.EntityState;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.sql.Types;

/**
 * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
 */
@Named
public class ReportService extends EntityService<Report, ReportDao> {

    private static final Logger LOG = LoggerFactory.getLogger(ReportService.class);

    @Inject
    private ReportDao reportDao;

    @Inject
    private EstablishmentDao establishmentDao;

    @Inject
    private MaternityDao maternityDao;

    @Inject
    private ReportDiscountTypeDao reportDiscountTypeDao;

    @Override
    protected ReportDao getDao() {
        return reportDao;
    }

    @Override
    public Report create(Report entity) {

        // 1. check duplicates
        checkDuplicates(entity);

        // 2. souche code
        updateSoucheCode(entity);

        // 3. check the establishment id
        checkEstablishmentId(entity);

        // 4. update activity id
        updateActivityId(entity);

        // 5. check the maternity id
        checkMaternityId(entity);

        // 6. update maternity id for specifics
        updateMaternityId(entity);

        // 7. discount
        checkReportDiscountType(entity);

        // 8. discount management


        return entity;
    }

    private void updateSoucheCode(Report report) {
        if (report.getSoucheCode() == null) {
            report.setSoucheCode(-1L);
        }
    }

    private void checkDuplicates(Report report) {
        long count = reportDao.countByStateAndTemporaryCode(EntityState.ACTIVE, report.getTemporaryCode());
        if (count > 0) {
            throw AppException.withMessage("Duplicates report with same temporary code").invalidArgs();
        }
    }

    private void checkEstablishmentId(Report report) {
        long count = establishmentDao.countByStateAndId(EntityState.ACTIVE, report.getEstablishmentCode());
        if (count != 1) {
            throw AppException.withMessage("Invalid establishment code").invalidArgs();
        }
    }

    private void checkMaternityId(Report report) {
        long count = maternityDao.countByStateAndId(EntityState.ACTIVE, report.getMaternityCode());
        if (count != 1) {
            throw AppException.withMessage("Invalid maternity code").invalidArgs();
        }
    }

    private void updateMaternityId(Report report) {
        if (report.getActivityId() == 104 || report.getActivityId() == 108 || report.getActivityId() == 112 || report.getActivityId() == 113) {
            if (StringUtils.equals(report.getClientContact().getContactCountry(), "BEL")) {
                report.setMaternityCode(444L);
            } else if (StringUtils.equals(report.getClientContact().getContactCountry(), "LUX")) {
                report.setMaternityCode(555L);
            } else {
                report.setMaternityCode(999687967L);
            }
        }
    }

    private void updateActivityId(Report report) {
        int value;
        switch (report.getActivityId().intValue()) {
            case 1:
                value = 101;
                break;
            case 2:
                value = 111;
                break;
            case 3:
                value = 110;
                break;
            case 4:
                value = 113;
                break;
            case 5:
                value = 104;
                break;
            case 6:
                value = 116;
                break;
            case 7:
                value = 112;
                break;
            case 8:
                value = 114;
                break;
            default:
                throw AppException.withMessage("Invalid activity id").invalidArgs();
        }

        report.setActivityId((long) value);
    }

    private void checkReportDiscountType(Report report) {
        long count = reportDiscountTypeDao.countById(report.getDiscountId());
        if (count != 1) {
            throw AppException.withMessage("Invalid discount id for report discount type").invalidArgs();
        }
    }

    private void updateForDiscount(Report report) {
        Long sellerId = null;
        long reportStateId = 25;
        long specialStatusId = 1;

        if (report.getEstablishmentCode() == 199 || report.getEstablishmentCode() == 198) {
            sellerId = report.getEstablishmentCode() == 199L ? 199L : 455L;
            reportStateId = 62;

            if (report.getDiscountId() == 1 || report.getDiscountId() == 3 || report.getDiscountId() == 7 ||
                    report.getDiscountId() == 2 || report.getDiscountId() == 6) {
                report.setUrgent(true);
            }
        } else {
            if (report.getDiscountId() == 1 || report.getDiscountId() == 3 || report.getDiscountId() == 7) {
                report.setUrgent(true);
                sellerId = 2L;
                reportStateId = 26;
                specialStatusId = report.getDiscountId();
            } else if (report.getDiscountId() == 2 || report.getDiscountId() == 6) {
                report.setUrgent(true);
            }
        }

        report.setSellerId(sellerId);
        report.setReportStateId(reportStateId);
//        report.
    }
}
