# API Baby

## Models

### Prospect client model

- firstname : string
- lastname : string
- address1 : string
- address2 : string
- address3 : string
- address4 : string
- postal_code : string
- city : string
- country : string
- home_phone_number : string
- mobile_phone_number : string
- profesionnal_phone_number : string
- email : string (required)
- gender : string enum {'MR', 'MME', 'MLLE', 'UNKNOWN'}
- optin_company : boolean
- optin_partners : boolean
- optout_company : boolean
- optout_partners : boolean
- baby : PropectBaby (optional, but when provided, below validation applies)


### Prospect baby model

- firstname : string (required)
- lastname : string (required)
- birth_date : date
- sexe : string enum {'MALE', 'FEMALE'} (required)
- rank : integer


### BirthStats

- maternity_code: integer (required)
- real_number: integer (required)


## API endpoints

### Create prospect

```
POST /prospect/client
```

##### Required params

- firstname
- lastname
- email

If baby prospect is provided, the following baby properties are required:

- firstname
- lastname


##### Response

- success: 200 and returns the prospect client json

##### Example

```
curl -X POST \
	-H "Content-Type: application/json" \
	-d '{"firstname":"Foo", "lastname":"Bar", "email": "lukasz@prima.com"}' \
	http://localhost:8080/prospect/client
```


### Prospects list

```
GET /prospect/clients
```

##### Required params

None.


##### Response

- success: 200 and returns the prospects list json

##### Example

```
curl -X GET \
	-H "Content-Type: application/json" \
	http://localhost:8080/prospect/clients
```


## Birth stats

### Save (create or update) birth stats

If the birth stats does not exists for the current month, it will create new entity.
Otherwise, it will update the current one.

For one entity:

```
POST /one/birthstats/month/{month}
POST /one/birthstats (= /one/birthstats/month/current)
```

For multiple entities:

```
POST /multiples/birthstats/month/{month}
POST /multiples/birthstats (= /multiples/birthstats/month/current)
```


- month (integer or string, optional, 'current' by default): two possible values
	- integer: the month of the birth stat, between 1 or 12
	- string: "current" for the current month

##### Response

- success: 200 and returns the entity json

##### Example

```
curl -X POST \
	-H "Content-Type: application/json" \
	-d '{"maternity_code":"1337", "real_number":"7331"}' \
	http://localhost:8080/birthstats/one

curl -X POST \
	-H "Content-Type: application/json" \
	-d '[{"maternity_code":"1337", "real_number":"7331"}, {"maternity_code":"1338", "real_number":"8331"}]' \
	http://localhost:8080/birthstats/multiples
```


### List birth stats for all maternities

```
GET /birthstats/month/{month}
GET /birthstats (= /birthstats/month/current)
```

- month (integer or string, optional, 'current' by default): two possible values
	- integer: the month of the birth stat, between 1 or 12
	- string: "current" for the current month

##### Response

- success: 200 and returns the json list


### List birth stats for specific maternity

```
GET /birthstats/maternity/{maternity}/month/{month}
GET /birthstats/maternity/{maternity} (= /birthstats/maternity/{maternity}/month/current)
```

- maternity (integer, required): the maternity code
- month (integer or string, optional, 'current' by default): two possible values
	- integer: the month of the birth stat, between 1 or 12
	- string: "current" for the current month

##### Response

- success: 200 and returns the json list