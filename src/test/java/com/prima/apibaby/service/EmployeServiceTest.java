//package com.prima.apibaby.service;
//
//import com.prima.apibaby.dao.EmployeeDao;
//import com.prima.apibaby.entity.employee.Employee;
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.transaction.TransactionConfiguration;
//import org.springframework.transaction.annotation.Transactional;
//
///**
// * @author Lukasz Piliszczuk <lukasz.pili AT gmail.com>
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"file:src/main/resources/applicationContext.xml"})
//@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
//@Transactional
//public class EmployeServiceTest {
//
//    @Autowired
//    private EmployeeDao employeeDao;
//
//    @Test
//    public void test() {
//        long id = 73658369848473L;
//        Employee employee = employeeDao.findOne(id);
//
//        Assert.assertNotNull(employee);
//        Assert.assertEquals(employee.getId(), Long.valueOf(id));
//    }
//}
